<!---->
  <div id="featured" >
    <ul class="ui-tabs-nav">
      <li class="ui-tabs-nav-item" id="nav-fragment-1"><a href="#fragment-1"><img src="<?php echo SITE_ROOT;?>img/index-content/index-content-small1.png" alt="" /><span></span></a></li>
      <li class="ui-tabs-nav-item" id="nav-fragment-3"><a href="#fragment-3"><img src="<?php echo SITE_ROOT;?>img/index-content/index-content-small3.png" alt="" /><span></span></a></li>
      <li class="ui-tabs-nav-item" id="nav-fragment-2"><a href="#fragment-2"><img src="<?php echo SITE_ROOT;?>img/index-content/index-content-small2.png" alt="" /><span></span></a></li>
      <li class="ui-tabs-nav-item" id="nav-fragment-4"><a href="#fragment-4"><img src="<?php echo SITE_ROOT;?>img/index-content/index-content-small4.png" alt="" /><span></span></a></li>
    </ul>

    <!-- First Content -->
    <div id="fragment-1" class="ui-tabs-panel" style="">
      <img src="<?php echo SITE_ROOT;?>img/index-content/index-content1.png" alt="" />
      <div class="info" >
        <h2><a href="#" >愛上Reage水波拉提</a></h2>
        <p>臉上各種細紋煩惱你嗎？相信我們，你將愛上Reage水波拉提！</p>
      </div>
    </div>

    <!-- Third Content -->
    <div id="fragment-3" class="ui-tabs-panel ui-tabs-hide" style="">
      <img src="<?php echo SITE_ROOT;?>img/index-content/index-content3.png" alt="" />
      <div class="info" >
        <h2><a href="#" >Reage線材大集合</a></h2>
        <p>Reage線材大集合，單股線、雙股螺旋線、雙側倒勾線。</p>
      </div>
    </div>
    

    <!-- Second Content -->
    <div id="fragment-2" class="ui-tabs-panel ui-tabs-hide" style="">
      <img src="<?php echo SITE_ROOT;?>img/index-content/index-content2.png" alt="" />
      <div class="info" >
        <h2><a href="#" >Reage水波拉提 - 素人故事</a></h2>
        <p>AMY透過水波拉提術，重新成為眾人的「嬌」點！全程記錄整個蝴蝶蛻變的過程！</p>
      </div>
    </div>

    
      <!-- Fourth Content -->
    <div id="fragment-4" class="ui-tabs-panel ui-tabs-hide" style="">
      <img src="<?php echo SITE_ROOT;?>img/index-content/index-content4.png" alt="" />
      <div class="info" >
        <h2><a href="#" >Reage水波拉提術 VS 其他各項緊膚治療</a></h2>
        <p>Reage水波拉提術與其他各項緊膚治療差在哪裡呢？</p>
      </div>
    </div>
  </div>
  <!---->
