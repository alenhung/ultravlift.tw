<footer class="">
	
	<div class="container whiteBg">
		<div class="footlinkArea"></div>
		<!--
		<div class="footlinkArea hidden-phone">
		
			<p class="footlink">
				<a herf="#">關於浩善</a> ｜
				<a herf="#">技術團隊</a> ｜
				<a herf="#">醫學研習會記錄</a> ｜
				<a herf="#">水波拉提術</a> ｜
				<a herf="#">水波拉提特色</a> ｜
				<a herf="#">產品規格</a>
			</p>
			<hr>
		
		</div>
	-->
		<div class="footWord pull-left">
			<ul>
				<li class="copyright-footer">copyright 2013 &#169; by Hao Shan Biotechnology Co.,Ltd . All Rights Reserved.</li>
				<!--
				<li>
					<a href="<?php echo SITE_ROOT?>contact.php">聯絡我們</a>
				</li>
				<li>
					聯絡電話：04-2326-0822
				</li>
				<li>
					聯絡信箱：<a href="mailto:service@haoshan.com.tw">service@haoshan.com.tw</a>
				</li>
			-->
				<!--
				<li>
					公司地址：台中市西屯區臺灣大道二段715號4樓之2
				</li>
			-->
			</ul>
		</div>
		<div class="pull-right footWord">
			<ul>
				<li>
					<img src="<?php echo SITE_ROOT; ?>img/logo_haoshan2.png" />
					<!--
					<a href="http://www.facebook.com/pages/%E6%B0%B4%E6%B3%A2%E6%8B%89%E6%8F%90/304628252973121" target="_blank"><img src="<?php echo SITE_ROOT;?>img/facebook-logo.png" alt="水波拉提官方粉絲團"></a>
					-->
				</li>
			</ul>
		</div>
	</div>
	<!-- /container -->
</footer>

<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="http://code.jquery.com/jquery-1.8.2.js"></script>
<!--<script src="<?php //echo SITE_ROOT; ?>js/featured-content-slider/js/jquery-1.8.2.js"</script>-->
<script src="<?php echo SITE_ROOT; ?>js/bootstrap.js"></script>
<script src="<?php echo SITE_ROOT; ?>js/siteInclude.js"></script>
<!--hover fropbox menu js-->
<script src="<?php echo SITE_ROOT; ?>js/twitter-bootstrap-hover-dropdown.min.js"></script>
<script>
	$(document).ready(function() {
		$(document).get(0).oncontextmenu = function() {
			return false;
		};
		$('.dropdown-toggle').dropdownHover(true);// active Menu mouse over
	});

</script>
	<!--Google Analytics Start-->
 <script type="text/javascript">
	    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-40594874-1', 'ultravlift.rw');
  ga('send', 'pageview');
	
	</script>
	<!--Google Analytics END-->

</body>
</html>