<?php
	require '../include/config.php';
	//載入樣板
	//header
	require '../template/tp_site_header.php';
	//navbar
	require '../template/tp_header.php';
	//navbar
	//require '../template/tp_navbar.php';
?>

<div class="container whiteBg">
  <div class="row">
    <div class="span3">
      <div class="contentBackgroundLeftGray">
        <img src="<?php echo SITE_ROOT;?>img/about/left-image.jpg" alt="">
      </div>
    </div>
    <div class="span9">
      <!---->
      <legend>
        關於浩善
      </legend>
      <p>愛美是天性，但我們更認為美麗是最原始的自信。浩善一直這樣相信著。</p>
      <p>過去浩善經營團隊已深耕醫學美容與整形外科產業十多年，深深瞭解人們為追求美麗的渴望與期盼！浩善生物科技引進或研發最好的產品與技術，創造更多美麗與自信。</p>
      <p>浩善期許自己成為醫學美容產品與技術的領導公司，團隊俱備完整醫學美容背景、整形外科技術、產品、技術研發等經驗，追求更美更極致的技術與產品是我們的使命，我們不惜成本至世界各地尋找最新醫美產品或技術，提供與世界頂級設備技術無縫接軌給予世人。</p>
      <div class="seminarDocument">
        <h4>市場定位</h4>
        <ol>
          <li>各項醫學美容產品技術引進、研發。</li>
          <li>各項整形外科產品技術引進、研發。</li>
          <li>各項醫學美容級保養品代理、研發。</li>
          <li>台灣線性拉提教學培訓單位</li>
          <li>韓國BS Medical 技術支援。</li>
        </ol>
      </div>
      <!---->
    </div>
  </div>
  
</div>
			
<?php
	require '../template/tp_footer.php';
?>
<script src="<?php echo SITE_ROOT;?>js/wavelift.js"></script>
<script>
	waveliftWhichNav(0);


</script>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 關於浩善" );
      $('#menu1 > li').eq(0).addClass('sideMenuActive');
		});
</script>