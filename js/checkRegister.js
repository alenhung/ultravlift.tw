/**
 * @author Alen Huang
 */
function validateEmail(email){
	var filter = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	if(filter.test(email)){
	     return true;
	}else{
	     return false;
	}
}
function inputEmpty(){
	var j = 0;
	$('.require').each(function(index){
		if(!$(this).find('input').val()){
			$(".require").eq(index).addClass('error');
			$(".require").eq(index).find('span').addClass('inputWrong');
			$(".require").eq(index).find('span').text('不可以空白');
			j+=1;
			
		}else{
			$(".require").eq(index).removeClass('error');
			$(".require").eq(index).find('span').removeClass('inputWrong');
			$(".require").eq(index).find('span').text('');
			j+=0;
		}
	});
	return j;
}
var Submit=function(){
    var URLs="script/function/register-finished.php";
   
    $.ajax({
        url: URLs,
        data: $('#registerform').serialize(),
        type:"POST",
        dataType:'html',

        success: function(msg){
           alert(msg);
           if(msg == 'true'){
           	  alert('msg-rtue');
			  //window.location.href = "index.php";
			
           }else{
           	alert('msg');
           	//alert('帳號或密碼錯誤！');
           	//$('#username').val('');
           	//$('#password').val('');
           }
        },

         error:function(xhr, ajaxOptions, thrownError){ 
            alert(xhr.status); 
            alert(thrownError); 
         }
    });
    
}
$(document).ready(function(){
	
	$('form').submit(function(){
		if(inputEmpty() >= 1){
			return false;
		}
		//
		
		/*
		if(!$(".require").find('input').val()){
			$(".require").addClass('error');
			$(this).find('span').text('不可以空白');
			return false;
		}
		*/
		/*
		if(!$('#username').val()){
			$('#usernameInput').addClass('error');
			$('#wrongUsername').addClass('inputWrong');
			$('#wrongUsername').text('不可以空白');		
		}
		
		if(!$('#username').val()){
			$('#passwordInput').addClass('error');
			$('#wrongPassword').addClass('inputWrong');
			$('#wrongPassword').text('不可以空白');		
		}
		
		if(!$('#realnameInput').val()){
			$('#realnameInput').addClass('error');
			$('#wrongRealname').addClass('inputWrong');
			$('#wrongRealname').text('不可以空白');		
		}
		
		if(!$('#telInput').val()){
			$('#telInput').addClass('error');
			$('#wrongTel').addClass('inputWrong');
			$('#wrongTel').text('不可以空白');		
		}
		
		if(!$('#emailInput').val()){
			$('#emailInput').addClass('error');
			$('#wrongEmail').addClass('inputWrong');
			$('#wrongEmail').text('不可以空白');		
		
		}
		*/
		//檢查密碼
		if($('#password').val() != $('#password2').val()){
			$('#passwordInput').addClass('error');
			$('#passwordInput2').addClass('error');
			$('#password').val('');
			$('#password2').val('');
			$('#wrongPassword').addClass('inputWrong');
			$('#wrongPassword').text('  兩次密碼不一樣');
			$('#wrongPassword2').addClass('inputWrong');
			$('#wrongPassword2').text('  兩次密碼不一樣');
			return false;
		};
		//檢查email
		if(validateEmail($('#email').val())){
			return true;
		}else{
			alert
			$('#emailInput').addClass('error');
			$('#wrongEmail').addClass('inputWrong');
			$('#wrongEmail').text('E-mail 格式錯誤');
			return false;
		};
		//Submit();
		//if(inputEmpty() == false){
			//return false;
		//}
		// alert(inputEmpty());
	});
	/*
	$('#password').change(function(){
		if($(this).val() == $('#password2').val()){
			$('#passwordInput').removeClass('error');
			$('#passwordInput2').removeClass('error');
			$('#wrongPassword').removeClass('inputWrong');
			$('#wrongPassword2').removeClass('inputWrong');
			$('#wrongPassword').text('');
			$('#wrongPassword2').text('');
		}
	});
	*/
	$('#password2').change(function(){
		if($(this).val() == $('#password').val()){
			$('#passwordInput').removeClass('error');
			$('#passwordInput2').removeClass('error');
			$('#wrongPassword').removeClass('inputWrong');
			$('#wrongPassword2').removeClass('inputWrong');
			$('#wrongPassword').text('');
			$('#wrongPassword2').text('');
		}
	});
	$('#tel').numeric();
	$('#email').change(function(){
		if(validateEmail($(this).val())){
			$('#emailInput').removeClass('error');
			$('#wrongEmail').removeClass('inputWrong');
			$('#wrongEmail').text('');
		}else{
			
		}	
	});
	$('#username').blur(function(){
		if(!$(this).val()){
		}
	});
});
