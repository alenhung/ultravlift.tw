//表單驗證用
//確認電子郵件函式
function validateEmail(email){
	var filter = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	if(filter.test(email)){
	     return true;
	}else{
	     return false;
	}
}
//確認require表單
function inputEmpty(){
	
	
	var j = 0;
	$('.require').each(function(index){
		if(!$(this).find('input').val()){
			$(".require").eq(index).addClass('error');
			$(".require").eq(index).find('span').addClass('inputWrong');
			$(".require").eq(index).find('span').text('不可以空白');
			j+=1;
		}else{
			$(".require").eq(index).removeClass('error');
			$(".require").eq(index).find('span').removeClass('inputWrong');
			$(".require").eq(index).find('span').text('');
			j+=0;
		}
	});
	return j;
}