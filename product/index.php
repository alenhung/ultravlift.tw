<?php
	require '../include/config.php';
	//載入樣板
	//header
	require '../template/tp_site_header.php';
	//navbar
	require '../template/tp_header.php';
?>

<div class="container whiteBg">
	<div class="row">
		<div class="span3">
			
		</div>
		<div class="span9">
			
			<legend>
				產品/服務
			</legend>
			<!---->
			<div class="row">
				<div class="span3">
					<div class="productBlock">
						<p>單股線 Mono thread</p>
						<img src="<?php echo SITE_ROOT;?>img/mono-icon.png" alt="">
						<hr>
						<a href="#" class="btn-in">更多訊息</a>
					</div>
				</div>
				<div class="span3">
					<div class="productBlock">
						<p>雙股線 Tornado thread</p>
						<img src="<?php echo SITE_ROOT;?>img/tornado-icon.png" alt="">
						<hr>
						<a href="#" class="btn-in">更多訊息</a>
					</div>
				</div>
				<div class="span3">
					<div class="productBlock">
						<p>倒勾線 Rose thread</p>
						<img src="<?php echo SITE_ROOT;?>img/rose-icon.png" alt="">
						<hr>
						<a href="#" class="btn-in">更多訊息</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	
		
	
	
</div>
<?php
require '../template/tp_footer.php';
?>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 課程安排" );
			$('.sideSubMenu').find('li').eq(0).addClass('sideMenuActive');
		});
</script>