<?php
	require '../include/config.php';
	//載入樣板
	//header
	require '../template/tp_site_header.php';
	//navbar
	require '../template/tp_header.php';
?>


<div class="container whiteBg">
	<ul class="breadcrumb">
  	<li><a href="<?php echo SITE_ROOT; ?>">首頁</a> <span class="divider">/</span></li>
  	<li><a href="<?php echo SITE_ROOT; ?>seminar/">研習會活動</a> <span class="divider">/</span></li>
    <li class="active"> 報名方式</li>
	</ul>
</div>
<div class="container whiteBg">
	<div class="row">
		<div id="contentSideMenuStyle" class="span3">
			<!--Sidebar Emnu-->
      <?php require'../template/tp_seminarSideMenuBar.php';?>
		</div>
		<div class="span9">
			<legend>
				報名方式
			</legend>
			<div class="heroIntro2">
				<p>
					內容整理中，敬請期待。
				</p>
			</div>
		</div>

	</div>

</div>
<?php
require '../template/tp_footer.php';
?>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 報名方式" );
			$('.sideSubMenu').find('li').eq().addClass('sideMenuActive');
		});
</script>