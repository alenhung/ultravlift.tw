<?php
	require '../include/config.php';
	//載入樣板
	//header
	require '../template/tp_site_header.php';
	//navbar
	require '../template/tp_header.php';
?>
<div class="container whiteBg">
	<?php //require '../template/tp_breadcrumb.php'; ?>
  <ul class="breadcrumb">
    <li><a href="<?php echo SITE_ROOT; ?>">首頁</a> <span class="divider">/</span></li>
    <li class=""> 案例介紹<span class="divider">/</span></li>
    <li class="active">雙下巴與頸部</li>
  </ul>
</div>
<div class="container whiteBg">
	<div class="row">
    <div id="contentSideMenuStyle" class="span3">
      <!--Sidebar Emnu-->
      <?php require'../template/tp_caseSideMenuBar.php';?>
    </div>
    <div class="span9">
    	<!--Body content-->
      <legend>雙下巴與頸部</legend>
      <h4>案例說明：V臉</h4>
      <img src="<?php echo SITE_ROOT;?>img/case/case3.png" alt="">
      <h4>案例說明：下顎線</h4>
      <img src="<?php echo SITE_ROOT;?>img/case/case4.png" alt="">
      <h4>案例說明：雙下巴</h4>
      <img src="<?php echo SITE_ROOT;?>img/case/case7.png" alt="">
      <h4>案例說明：頸紋</h4>
      <img src="<?php echo SITE_ROOT;?>img/case/case6.png" alt="">
      
    </div>
	</div>
</div>
	
			
<?php
	require '../template/tp_footer.php';
?>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 雙下巴與頸部案例介紹" );

      $('.sideSubMenu').find('li').eq(2).addClass('sideMenuActive');
		});
</script>