<?php
	require '../include/config.php';
	//載入樣板
	//header
	require '../template/tp_site_header.php';
	//navbar
	require '../template/tp_header.php';
?>
<div class="container whiteBg">
	<?php //require '../template/tp_breadcrumb.php'; ?>
  <ul class="breadcrumb">
    <li><a href="<?php echo SITE_ROOT; ?>">首頁</a> <span class="divider">/</span></li>
    <li class=""> 案例介紹<span class="divider">/</span></li>
    <li class="active"> 中下臉部</li>
  </ul>
</div>
<div class="container whiteBg">
	<div class="row">
    <div id="contentSideMenuStyle" class="span3">
      <!--Sidebar Emnu-->
      <?php require'../template/tp_caseSideMenuBar.php';?>
    </div>
    <div class="span9">
    	<!--Body content-->
      <legend>中下臉部</legend>
      <h4>案例說明：法令紋、木偶紋、嘴邊肉</h4>
      <img src="<?php echo SITE_ROOT;?>img/case/case1.png" alt="">
    </div>
	</div>
</div>
	
			
<?php
	require '../template/tp_footer.php';
?>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 中下臉部案例介紹" );

      $('.sideSubMenu').find('li').eq(1).addClass('sideMenuActive');
		});
</script>