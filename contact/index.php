<?php
	require '../include/config.php';
	//載入樣板
	//header
	require '../template/tp_site_header.php';
	//navbar
	require '../template/tp_header.php';
?>
<!--CONTENT START-->
<div class="container whiteBg">
	<div class="row">
		<div class="span4">
		</div>
		<div class="span8">
			<form action="../action/modify.php?type=contact_medical" method="post" accept-charset="utf-8" class="form-horizontal">
				<fieldset>
					<legend>
						聯絡我們
					</legend>

					<div class="control-group require" id="hospitalName">
						<label class="control-label">公司名稱：</label>
						<div class="controls ">
							<input type="text" placeholder="請輸入院所名稱" class="span4" name="companyName" id="companyName">
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require" >
						<label class="control-label">聯絡人：</label>
						<div class="controls">
							<input type="text" placeholder="請輸入聯絡人名稱" class="span4" name="contactName" id="contactName">
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">聯絡電話：</label>
						<div class="controls">
							<input type="tel" placeholder="請輸入聯絡電話" class="span4" name="contactTel" id="tel">
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">聯絡信箱：</label>
						<div class="controls">
							<input type="email" placeholder="請輸入聯絡信箱" class="span4" name="contactEmail" id="email">
							<span class="help-inline" id="wrongEmail"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">諮詢內容：</label>
						<div class="controls">
							<textarea rows="3" class="span4" name="comment" id="comment"></textarea>
						</div>
					</div>
					<div class="control-group">
    					<div class="controls">
    						<button type="submit" class="btn pull-left">送出</button>
    						<span class="help-inline"></span>
    					</div>
    				</div>	
				</fieldset>
			</form>
		</div>
		
	</div>
</div>
<!--CONTENT END-->
<?php
	require '../template/tp_footer.php';
?>
<script src="<?php echo SITE_ROOT;?>js/jquery.numeric.js"></script>
<script>
$(document).ready(function(){
	$( 'title' ).html ( "<?php echo SITE_NAME;?> - 醫療人員諮詢專頁" );
	$('.sideSubMenu').find('li').eq(0).addClass('sideMenuActive');
	$('#tel').numeric();
	$('#email').change(function(){
		if(validateEmail($(this).val())){
			$('#email').parent().parent().removeClass('error');
			$('#wrongEmail').removeClass('inputWrong');
			$('#wrongEmail').text('');
		}else{
			
		}	
	});
	$('form').submit(function(){
		if(inputEmpty() >= 1){
			return false;
		}
		if(validateEmail($('#email').val())){
			return true;
			}else{
				$('#email').parent().parent().addClass('error');
				$('#wrongEmail').text('E-mail 格式錯誤');
			return false;
		};
	});

	$("#optionsRadios1").click(function(){
  		$('#userlocation').hide();
		$('#userPosition').hide();
		$('#hospitalName').show();
		$('#hospitalName').addClass('require');
  	});
	$("#optionsRadios2").click(function(){
  		$('#userlocation').show();
		$('#userPosition').show();
		$('#hospitalName').hide();
		$('#hospitalName').removeClass('require');
  	});
});
</script>