-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- 主機: localhost
-- 產生日期: 2013 年 03 月 01 日 00:08
-- 伺服器版本: 5.5.27
-- PHP 版本: 5.3.15

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 資料庫: `haoshan`
--

-- --------------------------------------------------------

--
-- 表的結構 `centerDB`
--

CREATE TABLE IF NOT EXISTS `centerDB` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `centerArea` int(11) DEFAULT NULL,
  `centerName` varchar(45) DEFAULT NULL,
  `centerZipcode` varchar(45) DEFAULT NULL,
  `centerAddress` varchar(45) DEFAULT NULL,
  `centerDoctor` varchar(45) DEFAULT NULL,
  `centerContact` varchar(45) DEFAULT NULL,
  `centerTEL` varchar(45) DEFAULT NULL,
  `centercCell` varchar(45) DEFAULT NULL,
  `centerEmail` varchar(45) DEFAULT NULL,
  `centerWebsite` varchar(45) DEFAULT NULL,
  `centerProduct` varchar(45) DEFAULT NULL,
  `centerComment` text,
  `createUser` varchar(45) DEFAULT NULL,
  `createIP` varchar(45) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `changeTime` datetime DEFAULT NULL,
  `changeUser` varchar(45) DEFAULT NULL,
  `changeIP` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- 轉存資料表中的資料 `centerDB`
--

INSERT INTO `centerDB` (`id`, `centerArea`, `centerName`, `centerZipcode`, `centerAddress`, `centerDoctor`, `centerContact`, `centerTEL`, `centercCell`, `centerEmail`, `centerWebsite`, `centerProduct`, `centerComment`, `createUser`, `createIP`, `createTime`, `changeTime`, `changeUser`, `changeIP`) VALUES
(3, 1, '223全視創作', '242', '新北市新莊區新莊路231號一樓', '黃佑任', '黃佑任', '02-29919511', '0921106177', 'alenhung@gmail.com', 'http://quanshi.com.tw', '0,1,2', '沒有備註', 'alenhung', '127.0.0.1', '2013-02-06 20:13:44', '2013-02-10 03:37:27', 'test', '127.0.0.1'),
(4, NULL, 'jfjkfhuew', '2342', 'gkrmekgmrelgmrl', 'grrkmkg', 'grekmkgr', '04-23216757', '0921106178', 'alenhung@gmail.com', 'http://apple.com.tw', '0,2', 'gew', 'alenhung', '127.0.0.1', '2013-02-06 22:04:09', '2013-02-06 22:07:44', 'alenhung', '127.0.0.1'),
(5, 2, '號善', '3233', '32fewfew', 'fewfew', 'fwefew', '32432432', 'fewfew23432', 'fewfew3@feefw.few', '423432432', '0,2', 'fewfew', 'alenhung', '127.0.0.1', '2013-02-06 22:39:16', '2013-02-06 22:46:47', 'alenhung', '127.0.0.1'),
(6, 0, 'gweeg', 'gewg', 'ewg', 'ewgeds', 'nfggf', 'nuymuymuyn', 'ngfngf', 'ngfngf', 'ngfnfg', '1', 'trtr', 'alenhung', '127.0.0.1', '2013-02-10 04:41:51', '2013-02-10 04:41:51', 'alenhung', '127.0.0.1'),
(7, 2, '23432gregre', 'gre', 'gre', 'gre', 'gre', 'gre', 'gre', 'greg', 're', '0', 'gre', 'alenhung', '127.0.0.1', '2013-02-10 04:42:26', '2013-02-10 04:42:26', 'alenhung', '127.0.0.1'),
(8, 0, '1111fewfewmfwek', '2332', 'fewfefew', 'fewfew', 'fewfewfe', '3324234', '23423423', 'fewfew@fewfe.few', 'fwefewfewfew', '0', 'fewfewfew', 'alenhung', '127.0.0.1', '2013-02-12 03:41:10', '2013-02-12 03:41:10', 'alenhung', '127.0.0.1'),
(9, 0, '555', '323', 'fewfewfew', 'fewfew', 'fewfewf', '233332', '323232432', 'fewfew@fewfw.few', 'fewfewf', '0', '', 'alenhung', '127.0.0.1', '2013-02-12 03:42:57', '2013-02-12 03:42:57', 'alenhung', '127.0.0.1'),
(10, 0, '66555', '323', 'fewfewfew', 'fewfew', 'fewfewf', '233332', '323232432', 'fewfew@fewfw.few', 'fewfewf', '0', '', 'alenhung', '127.0.0.1', '2013-02-12 03:43:23', '2013-02-12 03:43:23', 'alenhung', '127.0.0.1'),
(11, 0, '88866555', '323', 'fewfewfew', 'fewfew', 'fewfewf', '233332', '323232432', 'fewfew@fewfw.few', 'fewfewf', '0', '', 'alenhung', '127.0.0.1', '2013-02-12 03:44:17', '2013-02-12 03:44:17', 'alenhung', '127.0.0.1'),
(12, 0, '---88866555', '323', 'fewfewfew', 'fewfew', 'fewfewf', '233332', '323232432', 'fewfew@fewfw.few', 'fewfewf', '0,1', '', 'alenhung', '127.0.0.1', '2013-02-12 03:45:49', '2013-02-12 03:45:49', 'alenhung', '127.0.0.1');

-- --------------------------------------------------------

--
-- 表的結構 `contact_form`
--

CREATE TABLE IF NOT EXISTS `contact_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyName` varchar(45) DEFAULT NULL,
  `contactName` varchar(45) NOT NULL,
  `contactTel` varchar(45) NOT NULL,
  `contactEmail` varchar(45) NOT NULL,
  `comment` text NOT NULL,
  `contactIP` varchar(45) NOT NULL,
  `createTime` time NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- 轉存資料表中的資料 `contact_form`
--

INSERT INTO `contact_form` (`id`, `companyName`, `contactName`, `contactTel`, `contactEmail`, `comment`, `contactIP`, `createTime`) VALUES
(1, 'test', 'test', 'test', 'alenhung@gmail.com', 'fewfwfewfe', '127.0.0.1', '12:52:59'),
(2, 'wfewfew', 'fewfew', 'fewfew32432', 'efwfewfwe', 'fewfewfewfe', '127.0.0.1', '17:55:46'),
(3, '', '', '', 'fewfewfew@fwefe.fewfe', '', '127.0.0.1', '14:52:47'),
(4, 'vdsvds', 'dsvvsvd', 'vdsvdsvds', 'vdsvds', 'vdsvdsvsd', '127.0.0.1', '15:01:51'),
(5, 'fewfew', 'fewfe', '32434', 'alenhung@gmail.com', 'fewfewfew', '127.0.0.1', '15:03:14'),
(6, '', '', '', '', '', '127.0.0.1', '15:06:21'),
(7, '', '', '', '', '', '127.0.0.1', '15:24:16');

-- --------------------------------------------------------

--
-- 表的結構 `sign_form`
--

CREATE TABLE IF NOT EXISTS `sign_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seminarName` varchar(45) NOT NULL,
  `chineseName` varchar(45) NOT NULL,
  `englidhName` varchar(45) NOT NULL,
  `hospital` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `telPhone` varchar(45) NOT NULL,
  `cellPhone` varchar(45) NOT NULL,
  `location` varchar(45) NOT NULL,
  `comment` text,
  `createTime` time NOT NULL,
  `contactIP` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- 轉存資料表中的資料 `sign_form`
--

INSERT INTO `sign_form` (`id`, `seminarName`, `chineseName`, `englidhName`, `hospital`, `email`, `telPhone`, `cellPhone`, `location`, `comment`, `createTime`, `contactIP`) VALUES
(1, '', '黃佑任', 'Huang You REN', '全視創作', 'alenhung@gmail.com', '02-2991-9511', '0921-106-177', '南區', 'test', '22:32:00', '127.0.0.1'),
(2, '台中市第一屆整外水波拉提術 研討會', 'alenhung', 'alenhung', 'alenhung', 'test@gamilc.om', 'ewgewg', '0234834038402', '', 'fwjfiejfiojewojfoe', '17:37:23', '127.0.0.1'),
(3, '研討會參加意願登記表', 'fewfew', 'fewfew', 'fewef', 'wfewfew', 'fefew', 'fewfewfew', '南區', 'fewfewfew', '17:44:26', '127.0.0.1'),
(4, '研討會參加意願登記表', 'wfew', 'fewfew', 'fewfew', 'fewfew', '343432432432', 'efewfefew', '中區', 'dsfdsfds', '17:45:16', '127.0.0.1'),
(5, '研討會參加意願登記表', 'gergre', 'gregre', 'gregre', 'gregre', 'gregre', 'gregregre', '北區', 'gregrgre', '17:45:42', '127.0.0.1');

-- --------------------------------------------------------

--
-- 表的結構 `siteAdminUser`
--

CREATE TABLE IF NOT EXISTS `siteAdminUser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `realName` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `level` int(11) NOT NULL,
  `createTime` datetime NOT NULL,
  `createIP` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- 轉存資料表中的資料 `siteAdminUser`
--

INSERT INTO `siteAdminUser` (`id`, `username`, `password`, `realName`, `email`, `level`, `createTime`, `createIP`) VALUES
(2, 'alenhung', '96ac9a11d94d8f982ba476aa4b5ef503', '黃佑任', 'alenhung@gmail.com', 3, '2012-07-00 00:01:00', '2013-01-28 21:02:59'),
(3, 'test', '098f6bcd4621d373cade4e832627b4f6', '一般業務帳號', 'test@test.com.tw', 1, '2013-02-10 03:24:38', '127.0.0.1');

-- --------------------------------------------------------

--
-- 表的結構 `supplierDB`
--

CREATE TABLE IF NOT EXISTS `supplierDB` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplierName` varchar(45) DEFAULT NULL,
  `supplierCountry` varchar(45) DEFAULT NULL,
  `supplierZipcode` varchar(45) DEFAULT NULL,
  `supplierAddress` varchar(45) DEFAULT NULL,
  `supplierContact` varchar(45) DEFAULT NULL,
  `supplierTEL` varchar(45) DEFAULT NULL,
  `supplierCell` varchar(45) DEFAULT NULL,
  `supplierEmail` varchar(45) DEFAULT NULL,
  `supplierWebsite` varchar(45) DEFAULT NULL,
  `supplierComment` text,
  `createUser` varchar(45) DEFAULT NULL,
  `createIP` varchar(45) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `changeUser` varchar(45) DEFAULT NULL,
  `changeIP` varchar(45) DEFAULT NULL,
  `changeTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 轉存資料表中的資料 `supplierDB`
--

INSERT INTO `supplierDB` (`id`, `supplierName`, `supplierCountry`, `supplierZipcode`, `supplierAddress`, `supplierContact`, `supplierTEL`, `supplierCell`, `supplierEmail`, `supplierWebsite`, `supplierComment`, `createUser`, `createIP`, `createTime`, `changeUser`, `changeIP`, `changeTime`) VALUES
(1, 'ttt', 'fewfwefewf', '233232', 'fewfew', 'supplierContact', '233232', '332323', 'fewfew@fewfew.few', 'fewfewfew', '', 'alenhung', '127.0.0.1', '2013-02-12 03:57:51', 'alenhung', '127.0.0.1', '2013-02-13 04:03:39'),
(2, 'aaavdgeee', 'China', '234gre', 'fewjifjiwejfiejfiegre', 'supplierContact', 'ggre0423243423432', 'ggg88693232320', 'hhhalenfuun@gmail.com', 'efefegjreijgirjgireji', 'gregre', 'alenhung', '127.0.0.1', '2013-02-13 04:04:33', 'alenhung', '127.0.0.1', '2013-02-13 04:05:08');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
