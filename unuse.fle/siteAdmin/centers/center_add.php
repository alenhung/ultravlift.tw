<?php

require "../../include/config.php";
require "../action/config.php";
require "../template/tp_header.php";
require "../template/tp_navbar.php";
?>

<div class="container siteStart">
	<?php
	require "tp_navbar.php";
	?>
</div>
<div class="container">
	<form action="../action/modify.php?type=center_add" method="post" accept-charset="utf-8" class=>
		<fieldset>
			<h3>新增技術合作中心</h3>
			<hr>
		</fieldset>
	
		<div class="row">
				<div class="span6">
					<div class="control-group">
						<label class="control-label">所屬區域：</label>
						<div class="controls">
							<label class="radio">
							  <input type="radio" name="optionsRadios" id="optionsRadios1" value="0" checked>
							  北區
							</label>
							<label class="radio">
							  <input type="radio" name="optionsRadios" id="optionsRadios1" value="1">
							  中區
							  </label>
							<label class="radio">
							  <input type="radio" name="optionsRadios" id="optionsRadios1" value="2">
							  南區
							  </label>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">合作中心名稱：</label>
						<div class="controls">
							<input type="text" placeholder="技術合作中心名稱"  name="centerName" id="centerName" class="span3">
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">郵遞區號：</label>
						<div class="controls">
							<input type="text" placeholder="郵遞區號"  name="centerZipcode" id="centerZipcode" class="span2" > |<a href="http://www.post.gov.tw/post/internet/f_searchzone/index.jsp?ID=12105" target="_blank">查詢郵遞區號</a>
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">中心地址：</label>
						<div class="controls">
							<input type="text" placeholder="技術合作中心地址"  name="centerAddress" id="centerAddress" class="span3" >
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">主治醫師：</label>
						<div class="controls">
							<input type="text" placeholder="技術合作中心主治醫師"  name="centerDoctor" id="centerDoctor" class="span3"  >
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">聯絡人員：</label>
						<div class="controls">
							<input type="text" placeholder="技術合作中心聯絡者"  name="centerContact" id="centerContact" class="span3"  >
							<span class="help-inline"></span>
						</div>
					</div>
				</div>
				<div class="span6">
					<div class="control-group require">
						<label class="control-label">市內電話：</label>
						<div class="controls">
							<input type="text" placeholder="EX:04-23260822#123"  name="centerTEL" id="centerTEL" class="span3"  >
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">行動電話：</label>
						<div class="controls">
							<input type="text" placeholder="行動電話"  name="centercCell" id="centercCell" class="span3"  >
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">電子信箱：</label>
						<div class="controls">
							<input type="text" placeholder="電子信箱"  name="centerEmail" id="centerEmail" class="span3"  >
							<span class="help-inline" id="wrongEmail"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">網站網址：</label>
						<div class="controls">
							<input type="text" placeholder="EX:http://reage.com.tw"  name="centerWebsite" id="centerWebsite" class="span3"  >
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">合作產品：</label>
						<div class="controls">
							<label class="checkbox">
								<input type="checkbox" name="centerProduct[]" id="centerProduct1" value="0" checked/>
								水波拉提
							</label>
							<label class="checkbox">
								<input type="checkbox" name="centerProduct[]" id="centerProduct2" value="1" />
								DR B
							</label>
							<label class="checkbox">
								<input type="checkbox" name="centerProduct[]" id="centerProduct3" value="2" />
								others
							</label>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">其他備註：</label>
						<div class="controls">
							<textarea rows="3" class="span3" name="centerComment" id="centerComment"><?php echo $list['centerComment']; ?></textarea>
						</div>
					</div>
				</div>
			</div>
		<hr />
		<div class="pull-right">
			<button type="submit" class="btn btn-primary">送出</button>
		</div>
	</form>
</div>
<?php
require "../template/tp_footer.php";
?>
<script src="../../js/jquery.numeric.js"></script>
<script src="../../js/siteInclude.js"></script>
<script>
	//管理nav標示
	siteAdminWhichNav(1);
	//表單驗證用
	$(document).ready(function() {
		// Stuff to do as soon as the DOM is ready;
		$('#centerTEL').numeric();
		$('#centercCell').numeric();
		$('#centerZipcode').numeric();
		
		$('#centerEmail').change(function(){
			if(validateEmail($(this).val())){
				$('#centerEmail').parent().parent().removeClass('error');
				$('#wrongEmail').text('');
			}else{
				
			}	
		});
		$('form').submit(function(){
			if(inputEmpty() >= 1){
				return false;
			}
			if(validateEmail($('#centerEmail').val())){
				return true;
				}else{
					$('#centerEmail').parent().parent().addClass('error');
					$('#wrongEmail').text('E-mail 格式錯誤');
				return false;
			};s
		});
	});
</script>