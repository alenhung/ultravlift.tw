<?php
$type 	=  'center_list';
//
require "../../include/config.php";
require "../action/config.php";
require "../template/tp_header.php";
require "../template/tp_navbar.php";
?>

<div class="container siteStart">
	<?php
	require "tp_navbar.php";
	?>
	
	<div class="row">
		<div class="span12" id="alertArea"></div>
		<div class="span12">
			<legend>合作中心列表<small class="pull-right">共計 <?php echo $count?> 筆資料</small></legend>
			<table class="table table-hover table-striped">
				<thead>
					<tr>
						<th>區域</th>
						<th>中心名稱</th>
						<th>主治醫師</th>
						<th>聯絡電話</th>
						<th>電子信箱</th>
						<th>詳細資訊</th>
					</tr>
				</thead>
				<tbody>
					<?php while($list = mysql_fetch_assoc($result)) { ?>
						<?php
						$centerPlace = $list['centerArea'];
						switch ($centerPlace) {
							case 0:
								$centerPlace = CENTER_LOCATION_1;
								break;
							
							case 1:
								$centerPlace = CENTER_LOCATION_2;
								break;
							
							case 2:
								$centerPlace = CENTER_LOCATION_3;
								break;
							
							default:
								
								break;
						}
						?>
					<tr>
						<td><?php echo $centerPlace;?></td>
						<td><?php echo $list['centerName']?></td>
						<td><?php echo $list['centerDoctor']?></td>
						<td><?php echo $list['centerTEL']?></td>
						<td><?php echo $list['centerEmail']?></td>
						<td><a class="btn btn-info" type="button"  href="center_view.php?centerID=<?php echo $list['id']; ?>">詳細資料</a></td>
					</tr>
					<?php } ?>	
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php
require "../template/tp_footer.php";
?>
<script>
	siteAdminWhichNav(0);
</script>