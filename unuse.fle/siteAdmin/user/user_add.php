<?php
require "../../include/config.php";
require "../action/config.php";
//檢查使用者權限
require "../action/level_check.php";
require "../template/tp_header.php";
require "../template/tp_navbar.php";
?>
<div class="container siteStart">
	
	<form action="../action/modify.php?type=user_add" method="post" accept-charset="utf-8" class="well">
		<fieldset>
			<h3>新增管理者</h3>
		</fieldset>
		<div class="row">
			<div class="span12">
				<div class="control-group">
					<div class="controls">
						<input type="text" placeholder="管理者帳號"  name="username" id="loginName" class="span3">
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="password" placeholder="管理者密碼"  name="password" id="loginPassword" class="span3">
					</div>	
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="password" placeholder="請在輸入一次密碼"  name="password2" id="loginPassword" class="span3">
					</div>	
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="text" placeholder="真實姓名"  name="realName" id="realName" class="span3">
					</div>	
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="email" placeholder="電子信箱"  name="email" id="email" class="span3">
					</div>	
				</div>
				<div class="control-group">
					<div class="controls">
						<select name="level">
						  <option value="1">資料維護管理員者</option>
						  <option value="2">廠商管理者</option>
						  <option value="3">網站管理者</option>
						</select>
					</div>	
				</div>
			</div>
		</div>
		
		<hr />
		<button type="submit" class="btn">送出</button>
	</form>
			
</div>
<?php
require "../template/tp_footer.php";
?>