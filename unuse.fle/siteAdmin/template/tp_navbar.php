
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a>
          <a href="<?php echo ADMIN_ROOT;?>" class="brand"><?php echo SITE_NAME;?> - 網站維護控制端</a>
      <div class="nav-collapse">
        <ul class="nav haoshan-nav pull-right">
          <!--
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 關於浩善 <b class="caret"></b> </a>
            <ul class="dropdown-menu">
              <li>
                <a href="<?php echo SITE_ROOT; ?>company/">簡介</a>
              </li>
              <li>
                <a href="<?php echo SITE_ROOT; ?>company/consultant.php">醫師顧問</a>
              </li>
            </ul>
          </li>
          -->
          <li>
            <a href="#">歡迎使用：<?php echo $createUser;?></a>
          </li>
          <li>
              <a href="<?php echo ADMIN_ROOT;?>include/logout.php">登出</a>
          </li>
          
          <li></li>
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </div>
</div>
