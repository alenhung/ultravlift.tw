<div class="row">
	<div class="span3">
		<h4>代理廠商</h4>
		<p>新增、修改、檢視代理廠商相關資料</p>
		<a href="<?php echo ADMIN_ROOT;?>supplier/supplier_list.php" class="btn btn-primary">進入</a>
	</div>
	<div class="span3"></div>
	<div class="span3"></div>
	<div class="span3">
		<h4>使用者管理</h4>
		<p>新增管理者</p>
		<a href="<?php echo ADMIN_ROOT;?>user/user_list.php" class="btn btn-primary">進入</a>
	</div>
</div>