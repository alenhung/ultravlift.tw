<meta charset="utf-8" />
<?php
	require 'config.php';
	session_start();
	//將session清空
	unset($_SESSION['userID']);
	unset($_SESSION['username']);
	unset($_SESSION['fullName']);
	unset($_SESSION['level']);
	echo '<script> alert("您已登出！");location.href="'.ADMIN_ROOT.'login.php"</script>';
?>