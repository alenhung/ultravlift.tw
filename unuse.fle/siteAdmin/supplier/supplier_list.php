<?php


$type 	=  'supplier_list';
//
require "../../include/config.php";
require "../action/config.php";
//檢查使用者權限
require "../action/level_check.php";
//樣板
require "../template/tp_header.php";
require "../template/tp_navbar.php";
?>

<div class="container siteStart">
	<?php
	require "tp_navbar.php";
	?>
	
	<div class="row">
		<div class="span12" id="alertArea"></div>
		<div class="span12">
			<legend>代理廠商列表<small class="pull-right">共計 <?php echo $count?> 筆資料</small></legend>
			<table class="table table-hover table-striped">
				<thead>
					<tr>
						<th>國家</th>
						<th>代理廠商名稱</th>
						<th>聯絡人</th>
						<th>聯絡電話</th>
						<th>電子信箱</th>
						<th>詳細資訊</th>
					</tr>
				</thead>
				<tbody>
					<?php while($list = mysql_fetch_assoc($result)) { ?>
					<tr>
						<td><?php echo $list['supplierCountry'];?></td>
						<td><?php echo $list['supplierName'];?></td>
						<td><?php echo $list['supplierContact'];?></td>
						<td><?php echo $list['supplierTEL'];?></td>
						<td><?php echo $list['supplierEmail'];?></td>
						<td><a class="btn btn-info" type="button"  href="supplier_view.php?supplierID=<?php echo $list['id']; ?>">詳細資料</a></td>
					</tr>
					<?php } ?>	
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php
require "../template/tp_footer.php";
?>
<script>
	siteAdminWhichNav(0);
</script>