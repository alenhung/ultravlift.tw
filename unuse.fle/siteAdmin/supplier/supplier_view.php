<?php
$type 	=  'supplier_view';
//
require "../../include/config.php";
require "../action/config.php";
//檢查使用者權限
require "../action/level_check.php";
//樣版
require "../template/tp_header.php";
require "../template/tp_navbar.php";
?>

<div class="container siteStart">
	<?php
	require "tp_navbar.php";
	?>
	
	<?php $list = mysql_fetch_assoc($result) ?>
	<div class="btn-group pull-right">
	  <a href="supplier_list.php" class="btn">返回列表</a>
	  <a id="update" href="#" class="btn btn-primary">修改資料</a>
	  <a id="delete" href="../action/modify.php?type=supplier_delete&deleteID=<?php echo $list['id']; ?>" class="btn btn-danger">刪除此資料</a>
	</div>
	<!------------------------------------------------------------檢視資料------------------------------------------------------------>
	<div id="viewData" class="row">
		<div class="span12">
			<legend><?php echo $list['supplierCountry']; ?> - <?php echo $list['supplierName']; ?><small class="pull-right">建立日期： <?php echo $list['createTime']; ?> | 建立資料：<?php echo $list['createUser']; ?> </small></legend>
			<dl class="dl-horizontal">
			  <dt>國家</dt>
			  <dd><?php echo $list['supplierCountry']; ?></dd>
			  <dt>廠商名稱</dt>
			  <dd><?php echo $list['supplierName']; ?></dd>
			  <dt>廠商郵遞區號</dt>
			  <dd><?php echo $list['supplierZipcode']; ?></dd>
			  <dt>廠商地址：</dt>
			  <dd><?php echo $list['supplierAddress']; ?></dd>
			  <dt>聯絡人員：</dt>
			  <dd><?php echo $list['supplierContact']; ?></dd>
			  <dt>聯絡電話：</dt>
			  <dd><?php echo $list['supplierTEL']; ?></dd>
			  <dt>廠商行動電話：</dt>
			  <dd><?php echo $list['supplierCell']; ?></dd>
			  <dt>電子信箱：</dt>
			  <dd><?php echo $list['supplierEmail']; ?></dd>
			  <dt>網站網址：</dt>
			  <dd><?php echo $list['supplierWebsite']; ?></dd>
			  <dt>其他備註：</dt>
			  <dd><?php echo $list['supplierComment']; ?></dd>
			  <!--
			  <dt>登入ip</dt>
			  <dd><?php echo $list['createIP']; ?></dd>
				-->
			</dl>
		</div>
	</div>
	<!------------------------------------------------------------修改資料------------------------------------------------------------>
	<div id="updateData">
		<form action="../action/modify.php?type=supplier_update&updateID=<?php echo $list['id']; ?>" method="post" accept-charset="utf-8">
			<fieldset>
				<h3>修改技術合作中心</h3>
			</fieldset>
			<div class="row">
				<div class="span6">
					<div class="control-group require">
						<label class="control-label">廠商名稱：</label>
						<div class="controls">
							<input type="text" placeholder="廠商名稱"  name="supplierName" id="supplierName" class="span3" value="<?php echo $list['supplierName']; ?>">
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">廠商國家：</label>
						<div class="controls">
							<input type="text" placeholder="廠商國家"  name="supplierCountry" id="supplierCountry" class="span3" value="<?php echo $list['supplierCountry']; ?>">
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">廠商郵遞區號：</label>
						<div class="controls">
							<input type="text" placeholder="廠商郵遞區號"  name="supplierZipcode" id="supplierZipcode" class="span3" value="<?php echo $list['supplierZipcode']; ?>">
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">廠商地址：</label>
						<div class="controls">
							<input type="text" placeholder="廠商地址"  name="supplierAddress" id="supplierAddress" class="span3" value="<?php echo $list['supplierAddress']; ?>">
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">聯絡人員：</label>
						<div class="controls">
							<input type="text" placeholder="廠商業務聯絡者"  name="supplierContact" id="supplierContact" class="span3" value="<?php echo $list['supplierContact']; ?>">
							<span class="help-inline"></span>
						</div>
					</div>
				</div>
				<div class="span6">
					<div class="control-group require">
						<label class="control-label">聯絡電話：</label>
						<div class="controls">
							<input type="text" placeholder="廠商電話請加上國碼(ex:+886-4-2326--822)"  name="supplierTEL" id="supplierTEL" class="span3" value="<?php echo $list['supplierTEL']; ?>" >
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">廠商行動電話：</label>
						<div class="controls">
							<input type="text" placeholder="廠商行動電話請加上國碼(ex:+886-921-106-177)"  name="supplierCell" id="supplierCell" class="span3"value="<?php echo $list['supplierCell']; ?>">
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">電子信箱：</label>
						<div class="controls">
							<input type="text" placeholder="廠商電子信箱"  name="supplierEmail" id="supplierEmail" class="span3"value="<?php echo $list['supplierEmail']; ?>">
							<span class="help-inline" id="wrongEmail"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">網站網址：</label>
						<div class="controls">
							<input type="text" placeholder="廠商網站網址(http://reage.com.tw)"  name="supplierWebsite" id="supplierWebsite" class="span3"value="<?php echo $list['supplierWebsite']; ?>">
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">其他備註：</label>
						<div class="controls">
							<textarea rows="3" class="span3" name="supplierComment" id="supplierComment"><?php echo $list['supplierComment']; ?></textarea>
						</div>
					</div>
				</div>
		</div>
			<hr />
			<div class="pull-right">
			<a href="#" id="cancelUpdate" class="btn ">取消修改</a>  ｜
			<button type="submit" class="btn btn-primary">確認修改</button>
			</div>
		</form>
	</div>
</div>
<?php
require "../template/tp_footer.php";
?>

<script src="../../js/jquery.numeric.js"></script>
<script src="../../js/siteInclude.js"></script>
<script>
	siteAdminWhichNav(0);
	//表單驗證用
	$(document).ready(function() {
		// Stuff to do as soon as the DOM is ready;
		$('#supplierTEL').numeric();
		$('#supplierCell').numeric();
		$('#supplierZipcode').numeric();
		
		$('#supplierEmail').change(function(){
			if(validateEmail($(this).val())){
				$('#supplierEmail').parent().parent().removeClass('error');
				$('#wrongEmail').text('');
			}else{
				
			}	
		});
		$('form').submit(function(){
			if(inputEmpty() >= 1){
				return false;
			}
			if(validateEmail($('#supplierEmail').val())){
				return true;
				}else{
					$('#supplierEmail').parent().parent().addClass('error');
					$('#wrongEmail').text('E-mail 格式錯誤');
				return false;
			};s
		});
	});
</script>