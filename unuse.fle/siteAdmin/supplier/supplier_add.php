<?php

require "../../include/config.php";
require "../action/config.php";
//檢查使用者權限
require "../action/level_check.php";
//樣版
require "../template/tp_header.php";
require "../template/tp_navbar.php";
?>

<div class="container siteStart">
	<?php
	require "tp_navbar.php";
	?>
</div>
<div class="container">
	<form action="../action/modify.php?type=supplier_add" method="post" accept-charset="utf-8" >
		<fieldset>
			<h3>新增代理廠商</h3>
		</fieldset>
	
		<div class="row">
				<div class="span6">
					<div class="control-group require">
						<label class="control-label">廠商名稱：</label>
						<div class="controls">
							<input type="text" placeholder="廠商名稱"  name="supplierName" id="supplierName" class="span3">
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">廠商國家：</label>
						<div class="controls">
							<input type="text" placeholder="廠商國家"  name="supplierCountry" id="supplierCountry" class="span3">
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">廠商郵遞區號：</label>
						<div class="controls">
							<input type="text" placeholder="廠商郵遞區號"  name="supplierZipcode" id="supplierZipcode" class="span3" >
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">廠商地址：</label>
						<div class="controls">
							<input type="text" placeholder="廠商地址"  name="supplierAddress" id="supplierAddress" class="span3" >
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">聯絡人員：</label>
						<div class="controls">
							<input type="text" placeholder="廠商業務聯絡者"  name="supplierContact" id="supplierContact" class="span3"  >
							<span class="help-inline"></span>
						</div>
					</div>
				</div>
				<div class="span6">
					<div class="control-group require">
						<label class="control-label">聯絡電話：</label>
						<div class="controls">
							<input type="text" placeholder="廠商電話請加上國碼(ex:+886-4-2326--822)"  name="supplierTEL" id="supplierTEL" class="span3">
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">廠商行動電話：</label>
						<div class="controls">
							<input type="text" placeholder="廠商行動電話請加上國碼(ex:+886-921-106-177)"  name="supplierCell" id="supplierCell" class="span3">
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">電子信箱：</label>
						<div class="controls">
							<input type="text" placeholder="廠商電子信箱"  name="supplierEmail" id="supplierEmail" class="span3">
							<span class="help-inline" id="wrongEmail"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">網站網址：</label>
						<div class="controls">
							<input type="text" placeholder="廠商網站網址(http://reage.com.tw)"  name="supplierWebsite" id="supplierWebsite" class="span3">
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">其他備註：</label>
						<div class="controls">
							<textarea rows="3" class="span3" name="supplierComment" id="supplierComment"></textarea>
						</div>
					</div>
				</div>
		</div>
		<hr />
		<div class="pull-right">
			<button type="submit" class="btn btn-primary">儲存</button>
		</div>
	</form>
			
</div>
<?php
require "../template/tp_footer.php";
?>

<script src="../../js/jquery.numeric.js"></script>
<script src="../../js/siteInclude.js"></script>
<script>
	//管理nav標示
	siteAdminWhichNav(1);
	//表單驗證用
	$(document).ready(function() {
		// Stuff to do as soon as the DOM is ready;
		$('#supplierTEL').numeric();
		$('#supplierCell').numeric();
		$('#supplierZipcode').numeric();
		
		$('#supplierEmail').change(function(){
			if(validateEmail($(this).val())){
				$('#supplierEmail').parent().parent().removeClass('error');
				$('#wrongEmail').text('');
			}else{
				
			}	
		});
		$('form').submit(function(){
			if(inputEmpty() >= 1){
				return false;
			}
			if(validateEmail($('#supplierEmail').val())){
				return true;
				}else{
					$('#supplierEmail').parent().parent().addClass('error');
					$('#wrongEmail').text('E-mail 格式錯誤');
				return false;
			};s
		});
	});
</script>