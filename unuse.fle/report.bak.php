<?php
	require '../include/config.php';
	//載入樣板
	//header
	require '../template/tp_site_header.php';
	//navbar
	require '../template/tp_header.php';
?>
<style type="text/css">
  /* Sidenav for Docs
-------------------------------------------------- */

.bs-docs-sidenav {
  width: 228px;
  margin: 30px 0 0;
  padding: 0;
  background-color: #fff;
  -webkit-border-radius: 6px;
     -moz-border-radius: 6px;
          border-radius: 6px;
  -webkit-box-shadow: 0 1px 4px rgba(0,0,0,.065);
     -moz-box-shadow: 0 1px 4px rgba(0,0,0,.065);
          box-shadow: 0 1px 4px rgba(0,0,0,.065);
}
.bs-docs-sidenav > li > a {
  display: block;
  width: 190px \9;
  margin: 0 0 -1px;
  padding: 8px 14px;
  border: 1px solid #e5e5e5;
}
.bs-docs-sidenav > li:first-child > a {
  -webkit-border-radius: 6px 6px 0 0;
     -moz-border-radius: 6px 6px 0 0;
          border-radius: 6px 6px 0 0;
}
.bs-docs-sidenav > li:last-child > a {
  -webkit-border-radius: 0 0 6px 6px;
     -moz-border-radius: 0 0 6px 6px;
          border-radius: 0 0 6px 6px;
}
.bs-docs-sidenav > .active > a {
  position: relative;
  z-index: 2;
  padding: 9px 15px;
  border: 0;
  text-shadow: 0 1px 0 rgba(0,0,0,.15);
  -webkit-box-shadow: inset 1px 0 0 rgba(0,0,0,.1), inset -1px 0 0 rgba(0,0,0,.1);
     -moz-box-shadow: inset 1px 0 0 rgba(0,0,0,.1), inset -1px 0 0 rgba(0,0,0,.1);
          box-shadow: inset 1px 0 0 rgba(0,0,0,.1), inset -1px 0 0 rgba(0,0,0,.1);
}
/* Chevrons */
.bs-docs-sidenav .icon-chevron-right {
  float: right;
  margin-top: 2px;
  margin-right: -6px;
  opacity: .25;
}
.bs-docs-sidenav > li > a:hover {
  background-color: #f5f5f5;
}
.bs-docs-sidenav a:hover .icon-chevron-right {
  opacity: .5;
}
.bs-docs-sidenav .active .icon-chevron-right,
.bs-docs-sidenav .active a:hover .icon-chevron-right {
  background-image: url(../img/glyphicons-halflings-white.png);
  opacity: 1;
}
.bs-docs-sidenav.affix {
  top: 40px;
}
.bs-docs-sidenav.affix-bottom {
  position: absolute;
  top: auto;
  bottom: 270px;
}



</style>
<link type="text/css" rel="stylesheet" href="../css/bootstrap-lightbox.min.css"></link>
<div class="container whiteBg">
  <ul class="breadcrumb">
    <li><a href="<?php echo SITE_ROOT; ?>">首頁</a> <span class="divider">/</span></li>
    <li><a href="<?php echo SITE_ROOT; ?>wavelift/">Reage 水波拉提術</a> <span class="divider">/</span></li>
    <li class=""> 媒體報導<span class="divider">/</span></li>
    <li class="active"> 媒體相關報導</li>
  </ul>
</div>
<div class="container whiteBg">
	<div class="row">
    <div id="contentSideMenuStyle" class="span3">
      <!--Sidebar Emnu-->
      <?php require'../template/tp_waveliftSideMenuBar.php';?>
    </div>
    <div class="span9">
    	<!--Body content-->
      <legend>媒體相關報導</legend>
      <!---->
      <!--縮圖開始-->
      <section id="first">
        <ul class="thumbnails">
          <li class="span3">
            <a data-toggle="lightbox" href="#magazine1" class="thumbnail">
              <img src="../img/magazine/drbeauty-02-small.jpg" alt="Click to view the lightbox">
            </a>
          </li>
          <li class="span3">
            <a data-toggle="lightbox" href="#magazine2" class="thumbnail">
              <img src="../img/magazine/drbeauty-03-small.jpg" alt="Click to view the lightbox">
            </a>
          </li>
          <li class="span3">
            <a data-toggle="lightbox" href="#magazine3" class="thumbnail">
              <img src="../img/magazine/drbeauty-04-small.jpg" alt="Click to view the lightbox">
            </a>
          </li>
          <li class="span3">
            <a data-toggle="lightbox" href="#magazine4" class="thumbnail">
              <img src="../img/magazine/psbeauty-01-small.jpg" alt="Click to view the lightbox">
            </a>
          </li>
          <li class="span3">
            <a data-toggle="lightbox" href="#magazine5" class="thumbnail">
              <img src="../img/magazine/psbeauty-02-small.jpg" alt="Click to view the lightbox">
            </a>
          </li>
        </ul>
      <!--縮圖結束-->
      <!--對應大圖-->
        <div id="magazine1" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
          <div class='lightbox-header'>
            <button type="button" class="close" data-dismiss="lightbox" aria-hidden="true">&times;</button>
          </div>
          <div class='lightbox-content'>
            <img src="../img/magazine/drbeauty-02-large.jpg">
            <div class="lightbox-caption"><p>醫美人二月份雜誌內容</p><span class="pull-right magazineDownload"><a href="<?php echo SITE_ROOT;?>img/magazine/drbeauty-february.pdf" target="_blank">PDF 檔案下載</a></span></div>
          </div>
        </div>
        <div id="magazine2" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
          <div class='lightbox-header'>
            <button type="button" class="close" data-dismiss="lightbox" aria-hidden="true">&times;</button>
          </div>
          <div class='lightbox-content'>
            <img src="../img/magazine/drbeauty-03-large.jpg">
            <div class="lightbox-caption"><p>醫美人三月份雜誌內容</p><span class="pull-right magazineDownload"><a href="<?php echo SITE_ROOT;?>img/magazine/drbeauty-march.pdf" target="_blank">PDF 檔案下載</a></span></div>
          </div>
        </div>
        <div id="magazine3" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
          <div class='lightbox-header'>
            <button type="button" class="close" data-dismiss="lightbox" aria-hidden="true">&times;</button>
          </div>
          <div class='lightbox-content'>
            <img src="../img/magazine/drbeauty-04-large.jpg">
            <div class="lightbox-caption"><p>醫美人四月份雜誌內容</p><span class="pull-right magazineDownload"><a href="<?php echo SITE_ROOT;?>img/magazine/drbeauty-april.pdf" target="_blank">PDF 檔案下載</a></span></div>
          </div>
        </div>
        <div id="magazine4" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
          <div class='lightbox-header'>
            <button type="button" class="close" data-dismiss="lightbox" aria-hidden="true">&times;</button>
          </div>
          <div class='lightbox-content'>
            <img src="../img/magazine/psbeauty-01-large.jpg">
            <div class="lightbox-caption"><p>整形達人2013春季刊雜誌內容</p><span class="pull-right magazineDownload"><a href="<?php echo SITE_ROOT;?>img/magazine/psbeauty-2013-spring.pdf" target="_blank">PDF 檔案下載</a></span></div>
          </div>
        </div>
        <div id="magazine5" class="lightbox hide fade"  tabindex="-1" role="dialog" aria-hidden="true">
          <div class='lightbox-header'>
            <button type="button" class="close" data-dismiss="lightbox" aria-hidden="true">&times;</button>
          </div>
          <div class='lightbox-content'>
            <img src="../img/magazine/psbeauty-02-large.jpg">
            <div class="lightbox-caption"><p>整形達人2013春季刊雜誌內容</p><span class="pull-right magazineDownload"><a href="<?php echo SITE_ROOT;?>img/magazine/psbeauty-2013-spring-1.pdf" target="_blank">PDF 檔案下載</a></span></div>
          </div>
        </div>
        

      <!--對應大圖結束-->
      </section>


      <!---->
    </div>
	</div>
</div>
	
			
<?php
	require '../template/tp_footer.php';
?>
<script src="<?php echo SITE_ROOT;?>js/bootstrap-lightbox.min.js"></script>
<script src="<?php echo SITE_ROOT;?>js/scale.fix.js"></script>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 媒體相關報導" );

      $('#menu4 > li').eq(0).addClass('sideMenuActive');
		});
</script>