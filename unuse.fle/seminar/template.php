<?php
require '../include/config.php';
//載入樣板
//header
require '../template/tp_header.php';
//navbar
require '../template/tp_navbar.php';
?>
<header class="siteHeader HeaderBlock">
	<div class="container">
		<h1>醫學研習會</h1>
	</div>
</header>
<div class="container ">
	<h2>Reage 水波拉提術醫學研習班 第一期</h2>
</div>
<div class="container">
	<div class="row">
		<div class="span6">
			<form action="action/modify.php?type=contact" method="post" accept-charset="utf-8" class="form-horizontal">
				<fieldset>
					<legend>
						線上報名表單
					</legend>
					<div class="control-group">
						<label class="control-label">中文姓名：</label>
						<div class="controls">
							<input type="text" placeholder="請輸入中文姓名" class="span4" name="companyName" id="companyName">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">英文名稱：</label>
						<div class="controls">
							<input type="text" placeholder="與護照英文名相同" class="span4" name="contactName" id="contactName">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">就職院所：</label>
						<div class="controls">
							<input type="text" placeholder="請輸入就職院所名稱" class="span4" name="contactTel" id="contactTel">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">電子信箱：</label>
						<div class="controls">
							<input type="email" placeholder="請輸入聯絡信箱" class="span4" name="contactEmail" id="contactEmail">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">聯絡電話：</label>
						<div class="controls">
							<input type="text" placeholder="例：04-2326-8022" class="span4" name="contactEmail" id="contactEmail">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">行動電話：</label>
						<div class="controls">
							<input type="tel" placeholder="例：0912345678" class="span4" name="contactEmail" id="contactEmail">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">其他備註：</label>
						<div class="controls">
							<textarea rows="3" class="span4" name="comment" id="comment"></textarea>
						</div>
					</div>

					<div class="control-group">
						<div class="controls">
							<hr />
							<button type="submit" class="btn pull-right">
								送出
							</button>
						</div>
					</div>

				</fieldset>
			</form>
		</div>
		<div class="span6">
			<legend>
				研習會內容
			</legend>
			<ol>
				<li>
					主辦單位： 浩善生物科技股份有限公司
				</li>
				<li>
					研習時間：102年1月30日 (星期三)下午1時
				</li>
				<li>
					研習地點：明田藝術美學診所 (台中市西區南屯路一段30號) <a href="https://maps.google.com.tw/maps?q=%E5%8F%B0%E4%B8%AD%E5%B8%82%E8%A5%BF%E5%8D%80%E5%8D%97%E5%B1%AF%E8%B7%AF%E4%B8%80%E6%AE%B530%E8%99%9F&hl=zh-TW&ie=UTF8&client=safari&oe=UTF-8&hnear=403%E5%8F%B0%E4%B8%AD%E5%B8%82%E8%A5%BF%E5%8D%80%E5%8D%97%E5%B1%AF%E8%B7%AF%E4%B8%80%E6%AE%B530%E8%99%9F&t=m&z=16&brcurrent=3,0x34693d12d30c4a9d:0x4f4b05370a88b42d,0,0x346917dff97922ef:0x87523ee47ea6447f" class="link" target="_blank">[MAP]</a>
				</li>
				<li>
					招生對象：臨床執業醫師
				</li>
				<li>
					洽詢專線：04-23713113
				</li>
				<li>
					報名地點：線上報名 或至 明田藝術美學診所
				</li>
			</ol>
			<hr />
			<a href="<?php echo SITE_ROOT;?>wavelift" class="btn btn-primary">關於水波拉提</a>
			<hr />
			<div class="info-content">
				<h1 class="oops">OOPS!</h1>
				<p class="">糟糕！來不及報名怎麼辦？沒關係請在這裡填寫研習意願登記表。</p>
				<a href="#" class="btn btn-info">研習意願登記表</a>
			</div>
		</div>

	</div>

</div>
<?php
require '../template/tp_footer.php';
?>

<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 醫學研習會" );
		});
</script>