<?php
require '../include/config.php';
//載入樣板
//header
require '../template/tp_header.php';
//navbar
require '../template/tp_navbar.php';
?>
<header class="siteHeader HeaderBlock">
	<div class="container">
		<h1>醫學研習會</h1>
	</div>
</header>
<div class="container ">
	<h2>台中市第一屆整外水波拉提術 研討會</h2>
</div>
<div class="container">
	<div class="row">
		<div class="span6">
			<form action="../action/modify.php?type=sign_add_1" method="post" accept-charset="utf-8" class="form-horizontal">
				<fieldset>
					<legend>
						線上報名表單
					</legend>
					<div class="control-group">
						
						<div class="controls">
							<input type="text" placeholder="研討會名稱" class="span4" name="seminarName" id="seminarName" style="display: none;" value="台中市第一屆整外水波拉提術 研討會">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">中文姓名：</label>
						<div class="controls">
							<input type="text" placeholder="請輸入中文姓名" class="span4" name="chineseName" id="companyName">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">英文名稱：</label>
						<div class="controls">
							<input type="text" placeholder="與護照英文名相同" class="span4" name="englidhName" id="contactName">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">就職院所：</label>
						<div class="controls">
							<input type="text" placeholder="請輸入就職院所名稱" class="span4" name="hospital" id="contactTel">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">電子信箱：</label>
						<div class="controls">
							<input type="email" placeholder="請輸入聯絡信箱" class="span4" name="email" id="contactEmail">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">聯絡電話：</label>
						<div class="controls">
							<input type="text" placeholder="例：04-2326-8022" class="span4" name="telPhone" id="contactEmail">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">行動電話：</label>
						<div class="controls">
							<input type="tel" placeholder="例：0912345678" class="span4" name="cellPhone" id="contactEmail">
						</div>
					</div>
					<!--
					<div class="control-group">
						<label class="control-label">研習地點：</label>
						<div class="controls">
							<label class="radio">
							  <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
							  北區（桃園以北）
							</label>
							<label class="radio">
							  <input type="radio" name="optionsRadios" id="optionsRadios1" value="option2">
							  中區（新竹以南至嘉義）
							  </label>
							<label class="radio">
							  <input type="radio" name="optionsRadios" id="optionsRadios1" value="option3">
							  南區（嘉義以南）
							  </label>
						</div>
					</div>
					-->
					<div class="control-group">
						<label class="control-label">其他備註：</label>
						<div class="controls">
							<textarea rows="3" class="span4" name="comment" id="comment"></textarea>
						</div>
					</div>

					<div class="control-group">
						<div class="controls">
							<hr />
							<button type="submit" class="btn pull-right">
								送出
							</button>
						</div>
					</div>

				</fieldset>
			</form>
		</div>
		<div class="span6">
			<legend>
				研習會內容
			</legend>
			<!-- <p>感謝您對於水波拉提術重視，我們非常重視您的參與意願！請您留下您的資料，我們將有專人與您聯繫。</p> -->
			
			<div class="seminarIntro">
				<h4>主講：賴炳文 整形外科醫師</h4>
				<p class="seminarIntroP">水波拉提創始醫師，藉由整形外科原理，配合彈性 reage 針頭，達到立即拉提效果並減少百分之九十五瘀青機會，有別於其他埋線廠商。</p>
			</div>
			<div class="seminarContent">
				<ol>
					<li>
						研習時間：民國102年二月十七日 （星期日） 下午一點三十分
					</li>
					<li>
						研習地點：明田美學藝術診所<br />地址：台中市西區西屯路一段30號（五權路與南屯路口）  <a href="https://maps.google.com.tw/maps?q=%E5%8F%B0%E4%B8%AD%E5%B8%82%E8%A5%BF%E5%8D%80%E5%8D%97%E5%B1%AF%E8%B7%AF%E4%B8%80%E6%AE%B530%E8%99%9F&hl=zh-TW&ie=UTF8&ll=24.132126,120.667552&spn=0.009027,0.008068&sll=24.132361,120.667595&brcurrent=3,0x34693d12d30c4a9d:0x4f4b05370a88b42d,0,0x346917dff97922ef:0x87523ee47ea6447f&hnear=403%E5%8F%B0%E4%B8%AD%E5%B8%82%E8%A5%BF%E5%8D%80%E5%8D%97%E5%B1%AF%E8%B7%AF%E4%B8%80%E6%AE%B530%E8%99%9F&t=m&z=17" target="_blank">MAP</a>
					</li>
					<li>
						參加資格：限整形外科醫師
					</li>
					<li>
						研討會內容：
						<ol>
							<li>水波拉提術原理介紹。</li>
							<li>水波拉提術操作技巧。</li>
							<li>現場DEMO操作教學，請自行準備 model。</li>
						</ol>
					</li>
					<li>
						收費方式：僅收線材費及教學費兩萬元（200支水波拉提線，demo用）
					</li>
					<li>
						洽詢專線：04-2326-8022
					</li>
				</ol>
			</div>
			<hr />
			<a href="<?php echo SITE_ROOT;?>wavelift" class="btn btn-primary">關於水波拉提</a>
			<hr />
			<div class="info-content">
				<h1 class="oops">OOPS!</h1>
				<p class="">糟糕！來不及報名怎麼辦？沒關係請在這裡填寫研習意願登記表。</p>
				<a href="<?php echo SITE_ROOT;?>seminar/normal.php" class="btn btn-info">研習意願登記表</a>
			</div>
		</div>

	</div>

</div>
<?php
require '../template/tp_footer.php';
?>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 醫學研習會" );
		});
</script>