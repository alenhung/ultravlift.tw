<?php
require '../include/config.php';
//載入樣板
//header
require '../template/tp_header.php';
//navbar
require '../template/tp_navbar.php';
?>
<header class="siteHeader HeaderBlock">
	<div class="container">
		<h1>醫學研習會</h1>
	</div>
</header>
<div class="container ">
	<h2>水波拉提術醫學研習會 意願登記表</h2>
</div>
<div class="container">
	<div class="row">
		<div class="span6">
			<form action="../action/modify.php?type=sign_add" method="post" accept-charset="utf-8" class="form-horizontal">
				<fieldset>
					<legend>
						線上登記表單
					</legend>
					<div class="control-group">
						
						<div class="controls">
							<input type="text" placeholder="研討會名稱" class="span4" name="seminarName" id="seminarName" style="display: none;" value="研討會參加意願登記表">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">中文姓名：</label>
						<div class="controls">
							<input type="text" placeholder="請輸入中文姓名" class="span4" name="chineseName" id="companyName">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">英文名稱：</label>
						<div class="controls">
							<input type="text" placeholder="與護照英文名相同" class="span4" name="englidhName" id="contactName">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">就職院所：</label>
						<div class="controls">
							<input type="text" placeholder="請輸入就職院所名稱" class="span4" name="hospital" id="contactTel">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">電子信箱：</label>
						<div class="controls">
							<input type="email" placeholder="請輸入聯絡信箱" class="span4" name="email" id="contactEmail">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">聯絡電話：</label>
						<div class="controls">
							<input type="text" placeholder="例：04-2326-8022" class="span4" name="telPhone" id="contactEmail">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">行動電話：</label>
						<div class="controls">
							<input type="tel" placeholder="例：0912345678" class="span4" name="cellPhone" id="contactEmail">
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label">研習地點：</label>
						<div class="controls">
							<label class="radio">
							  <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
							  北區（桃園以北）
							</label>
							<label class="radio">
							  <input type="radio" name="optionsRadios" id="optionsRadios1" value="option2">
							  中區（新竹以南至嘉義）
							  </label>
							<label class="radio">
							  <input type="radio" name="optionsRadios" id="optionsRadios1" value="option3">
							  南區（嘉義以南）
							  </label>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">預定月份：</label>
						<div class="controls">
							<select name="optionMonth">
							  <option value="mon">一月</option>
							  <option value="feb">二月</option>
							  <option value="mar">三月</option>
							  <option value="apr">四月</option>
							  <option value="may">五月</option>
							  <option value="jun">六月</option>
							  <option value="jui">七月</option>
							  <option value="aug">八月</option>
							  <option value="sep">九月</option>
							  <option value="oct">十月</option>
							  <option value="nov">十一月</option>
							  <option value="dec">十二月</option>
							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">預定日期：</label>
						<div class="controls">
							<select name="optionDate">
							  <option value="sun">星期日</option>
							  <option value="mon">星期一</option>
							  <option value="tue">星期二</option>
							  <option value="wed">星期三</option>
							  <option value="thu">星期四</option>
							  <option value="fri">星期五</option>
							  <option value="sat">星期六</option>
							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">其他備註：</label>
						<div class="controls">
							<textarea rows="3" class="span4" name="comment" id="comment"></textarea>
						</div>
					</div>

					<div class="control-group">
						<div class="controls">
							<hr />
							<button type="submit" class="btn pull-right">
								送出
							</button>
						</div>
					</div>

				</fieldset>
			</form>
		</div>
		<div class="span6">
			<legend>
				研習會內容
			</legend>
			<p>感謝您對於Reage水波拉提術重視，我們非常重視您的參與意願！請您留下您的資料，我們將有專人與您聯繫。</p>
			<ol>
				<li>
					研習時間：不定期舉辦
				</li>
				<li>
					研習地點：北區、中區、南區（將於舉辦前通知研習會地點）
				</li>
				<li>
					招生對象：臨床執業醫師
				</li>
				<li>
					洽詢專線：04-2326-8022
				</li>
			</ol>
			<hr />
			<a href="<?php echo SITE_ROOT;?>wavelift" class="btn btn-primary">關於水波拉提介紹</a>
			<!--
			<hr />
			<div class="info-content">
				<h1 class="oops">OOPS!</h1>
				<p class="">糟糕！來不及報名怎麼辦？沒關係請在這裡填寫研習意願登記表。</p>
				<a href="<?php echo SITE_ROOT;?>seminar/normal.php" class="btn btn-info">研習意願登記表</a>
			</div>
			-->
		</div>

	</div>

</div>
<?php
require '../template/tp_footer.php';
?>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 醫學研習會" );
		});
</script>