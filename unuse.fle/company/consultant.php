<?php
	require '../include/config.php';
	//載入樣板
	//header
	require '../template/tp_header.php';
	//navbar
	require '../template/tp_navbar.php';
?>
<header class="siteHeader">
	<div class="container content_words">
		<img src="../img/company-intro.png" alt="" />
				<hr />
				<h4>顧問醫師</h4>
		<div class="row">
			<div class="span6 pull-right">
				
				<h4>賴炳文<span class="EnglishName">/ Lai,Ping-Wen</span></h4>
				<hr />
				
				<ul>
					<li>高雄醫學院醫學系(1985.9~1992.6)</li>
					<li>高雄醫學院附設醫院實習醫師(1991.7~1992.6) </li>
					<li>長庚醫院外科住院醫師(1992.7~1994.6) </li>
					<li>長庚醫院整形外科住院醫師(1994.7~1996.6) </li>
					<li>長庚醫院整形外科總醫師(1996.7~1997.6) </li>
					<li>長庚醫院外傷科整形外科組主治醫師(1997.7~2001.12) </li>
					<li>林新醫院整形外科主任醫師(2002.2~2003.7) </li>
					<li>上海歐華醫療美容⾨診部院長(2011.07~2012.03)</li>
					<li>順風醫療集團新北林口旗艦店院長(2011.12~迄今)</li>
					<!-- <li>水波拉提術創造者</li> -->
				</ul>
				<h4>認證醫師</h4>
				<ul>
					<li>美國 Allergen Botox 注射</li>
					<li>美國 Mentor Breast Cohesive Implant </li>
					<li>美國 Thermage 電波拉⽪皮</li>
					<li>瑞典 Q-Med 玻尿酸注射</li>
					<li>瑞士 Anteis 玻尿酸注射</li>
					<li>瑞士 Injectnow Academy</li>
					<li>水波拉提術台灣技術指導認證醫師</li>
				</ul>
				<img src="../img/bs-appointment.png" alt="水波拉提台灣技術指導認證">
			</div>
			<div class="span6">
				<img src="../img/awen.jpg" alt="" class="img-outline"/>
			</div>
		</div>
	</div>
</header>
<?php
	require '../template/tp_footer.php';
?>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 顧問醫師 賴炳文" );
		});
</script>