<?php a:?>
<!DOCTYPE html>
<html> 
<head> 
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" /> 
  <title>Google Maps Multiple Markers</title> 
  <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
</head> 
<body>
  <div id="map" style="width: 500px; height: 400px;"></div>

  <script type="text/javascript">
    var locations = [
      ['Bondi Beach', -33.890542, 151.274856, 4],
      ['Coogee Beach', -33.923036, 151.259052, 5],
      ['Cronulla Beach', -34.028249, 151.157507, 3],
      ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
      ['Maroubra Beach', -33.950198, 151.259302, 1],
      ['全視創作',25.0349749, 121.45502710000005,8]
    ];

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: new google.maps.LatLng(-33.92, 151.25),//顯示地圖初始化
      //center: latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map,
        icon: 'a.png'
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent('<h1>'+locations[i][0]+'</h1>');//可加html
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>
  <script>
  //地址轉經緯度
  function getLatLngByAddr() {
    var geocoder = new google.maps.Geocoder();  //定義一個Geocoder物件
    geocoder.geocode(
        { address: '[新北市新莊區新莊路231號1樓]' },    //設定地址的字串
        function(results, status) {    //callback function
            if (status == google.maps.GeocoderStatus.OK) {    //判斷狀態
                alert(results[0].geometry.location);             //取得座標                                 
            } else {
                alert('Error');
            }
            }
       );
    }
    getLatLngByAddr();
  </script>
  <p>Alenhung</p>
</body>
</html>