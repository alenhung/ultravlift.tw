<?php
	require '../include/config.php';
	//載入樣板
	//header
	require '../template/tp_site_header.php';
	//navbar
	require '../template/tp_header.php';
	//navbar
	require '../template/tp_navbar.php';
?>

	<div class="container IntroDiv whiteBg">
		<img src="../img/wavelifting/intro-wavelifting.png" alt="" />
	</div>
<?php require '../template/tp_wavelift_navbar.php';?>
<!--
<div class="container whiteBg">
	<div class="ripple">
		<img src="../img/wavelifting/ripple.jpg" alt="" />
		<hr />
	</div>
</div>
-->
<!--mono-->
<div class="container  waveIntroWord">
	<h2>多種線材規格，為您量身訂製拉提方案</h2>
	<p>不同於市面上大多數的線性拉提術只用單一規格材料，Reage水波拉提術最大的特色就是針對您的個人要求、肌膚問題與部位差異，利用多種不同規格的線材組合來為您量身訂製美麗方案。</p>
</div>
<div class="container whiteBg waveproduct">
	<h2 class="ReageMono">單股平滑線 Reage Mono Thread</h2><hr>
	<p class="threadWord">透過膠原蛋白的增生效果，讓肌膚達到緊實、填補、拉提等效果。適用於輕至中度老化者全臉拉提，或是眼部、前額、臉頰等部位處理時。</p>
	<div class="row">
		<div class="span5">
			<img src="../img/reage1.png" alt="水波拉提-reage針頭" />
		</div>
		<div class="span7 mono1">
			<h2>獨具特色 業界同規格中最細針頭</h2>
			<p>目前市場同規格中唯有REAGE為30G針頭，極細的針頭利於施術的醫師入針更順利，並且將可減少施術過程中所造成的針孔傷口與瘀青產生。配合水波拉提術施術所造成的針孔傷口將於半小時左右即可消失。</p>			
		</div>
	</div>
	<div class="row">
		<div class="span7 mono2">
				<h2>極俱彈性與針頭特殊設計</h2>
				<p>傳統針頭的直線切割方式，雖便利入針順利但也容易刺破微血管造成出血。REAGE特殊導角切割與極俱彈性的設計，於施術過程中將有效避開血管，降低術後瘀清機率。因此成為水波拉提術不可或缺的利器。</p>
		</div>
		<div class="span5">
				<img src="../img/reage3.png" alt="" class="waveliftingImage"/>
		</div>
	</div>
	
</div>
<!--Tornado-->
<div class="container whiteBg waveproduct">
	<h2 class="ReageMono">雙股龍捲風 Reage tornado Thread</h2>
	<p class="threadWord">雙股纏繞極致工藝，提供比單股線更強的支撐與拉提力。適用於中至重度老化者，兩頰V臉拉提或是法令紋、嘴角紋等皺紋支撐。</p>
	<hr />
	<div class="row">
		<div class="span5">
			<img src="../img/tornado-1.png" alt="水波拉提-reage針頭" class="waveliftingImage"/>
		</div>
		<div class="span7 tornado1">
			<h2>極致工藝 雙股纏繞高超技術</h2>
			<p>目前市場目前僅此產品為PDO雙股纏繞設計，並螺旋纏繞於針頭。入針後擁有更大面積刺激膠原蛋白增生，提高緊實除皺效果。配合水波拉提術施術針對特定部位與適應症，提供比起一般單股PDO平滑線創造數倍緊實、除皺效果。</p>			
		</div>
	</div>
	<div class="row">
		<div class="span6 tornado2">
				<h2>雙股螺旋龍捲風</h2>
				<p>雙股螺旋設計，目的在於提供更大體積刺激膠原蛋白增生，除了提供兩倍單股線的體積外，更纏繞螺旋於針頭上，比起線材螺旋入針後更加緊密，集中PDO刺激效果，為最強緊實、除皺的最佳利器。</p>
		</div>
		<div class="span6">
				<img src="../img/wavelifting/tornado-line.png" alt="" class="waveliftingImage"/>
		</div>
	</div>
	
</div>
<!--ROSE-->
<div class="container whiteBg waveproduct">
	<h2 class="ReageMono">玫瑰雙側倒勾 Reage Rose Thread</h2>
	<p class="threadWord">提供最強的拉提效果，對於老化下垂嚴重時，用於臉頰、頸部等部位加強拉提效果。</p>
	<hr />
	<div class="row">
		<div class="span12">
			<img src="../img/rose-1.png" alt="水波拉提-reage針頭" class="waveliftingImage"/>
		</div>
	</div>
	<div class="row">
		<div class="span7">
			<h2>雙側線性倒勾 物理性拉提極致指標</h2>
			<p>內外一同擁有倒溝設計，雙側物理線性拉提加倍！</p>
		</div>
		<div class="span5">
			<img src="../img/rose-2.png" alt="水波拉提-reage針頭" class="waveliftingImage"/>
		</div>
	</div>

	<div class="row">
		<div class="span12">
			<img src="../img/rose-3.png" alt="水波拉提-reage針頭" class="waveliftingImage"/>
		</div>
	</div>
	
</div>
<?php
	require '../template/tp_footer.php';
?>
<script src="<?php echo SITE_ROOT;?>js/wavelift.js"></script>
<script>
	waveliftWhichNav(3);


</script>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 水波拉提" );
		});
</script>