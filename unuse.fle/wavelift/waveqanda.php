<?php
	require '../include/config.php';
	//載入樣板
	//header
	require '../template/tp_header.php';
	//navbar
	require '../template/tp_navbar.php';
?>
<header class="siteHeader">
	<div class="container">
		<img src="../img/site-indexlogo3.png" alt="" />
	</div>
</header>

<?php require '../template/tp_wavelift_navbar.php';?>

<!-- <div class="container contentStart">
	<div class="ripple">
		<img src="img/ripple2.jpg" alt="" />
		<hr />
	</div>
</div> -->
<div class="container contentStart">
	<div class="qandTitle">
		<img src="../img/QandATitle.png" alt="Q & A" />
	</div>
	<div class="row">
		<div class="span12">
			<ul class="QandA">
				<li class="question">REAGE水波拉提與其他線性拉提有何不同？</li>
				<li class="answer">REAGE水波拉提針頭為同規格最細的針頭設計，有效降低疼痛感與腫脹產生，配合水波拉提術植入的針孔痕跡半小內即會消失。</li>
			</ul>
			<ul class="QandA">
				<li class="question">何時可以看到拉提效果？</li>
				<li class="answer">在經由專業醫師施術後，拉提的初步效果立即可見，持續的膠原蛋白增生效果下，一至三個月後效果會更加明顯。</li>
			<ul class="QandA">
			</ul>	
				<li class="question">線是否會摸的出來？表情是否不自然？</li>
				<li class="answer">所植入可吸收的PDO線比髮絲還細，利用特殊水波拉提術，不會有表情僵硬與不自然的問題。</li>
			</ul>
			<ul>	
				<li class="question">與其他拉皮或緊膚治療最大的區別在哪裡？</li>
				<li class="answer">除了無痛感與無需手術麻醉外，REAGE水波拉提術係採用多點式水波拉提，除了有立即效果外，同時持續刺激膠原蛋白增生，可以達到長期回春的作用。比其他治療往往只有單點或數點的拉提力更具效果。</li>
			</ul>
			<ul class="QandA">	
				<li class="question">適合部位與預期效果</li>
				<li class="answer">改善兩頰鬆弛、V臉拉提、下顎線拉提、蘋果肌、提眉、鼻型修飾、淚溝、法令紋填補、雙下巴與頸紋等。</li>
			</ul>
		</div>
	</div>
</div>
<?php
	require '../template/tp_footer.php';
?>
<script src="<?php echo SITE_ROOT;?>js/wavelift.js"></script>
<script>
	waveliftWhichNav(6)
</script>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 水波拉提" );
		});
</script>