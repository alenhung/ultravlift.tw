<?php
	require '../include/config.php';
	//載入樣板
	//header
	require '../template/tp_header.php';
	//navbar
	require '../template/tp_navbar.php';
?>
<header class="siteHeader">
	<div class="container">
		<img src="../img/site-indexlogo5.png" alt="" />
	</div>
</header>
<?php require '../template/tp_wavelift_navbar.php';?>
<div class="container contentStart">
	<div class="row">
		<div class="span6"></div>
		<div class="span6"></div>
	</div>
	<h1>Reage系列</h1>
	<table class="table table-striped table-hover">
		<tr>
			<td>item</td>
			<td>needle length</td>
			<td>threads length</td>
		</tr>
		<tr>
			<td>R1 (30G)</td>
			<td>30mm</td>
			<td>30mm</td>
		</tr>
		<tr>
			<td>R2 (30G)</td>
			<td>40mm</td>
			<td>50mm</td>
		</tr>
		<tr>
			<td>R5 (29G)</td>
			<td>60mm</td>
			<td>90mm</td>
		</tr>
	</table>
	<hr />
	<h1>Dream UP系列</h1>
	<h3>Mono 平滑線</h3>
	<table class="table table-striped table-hover ">
		<tr>
			<td>item</td>
			<td>neddle length</td>
			<td>threads length</td>
		</tr>
		<tr>
			<td>D1 (29G)</td>
			<td>25.4mm</td>
			<td>30mm</td>
		</tr>
		<tr>
			<td>D2 (29G)</td>
			<td>38mm</td>
			<td>50mm</td>
		</tr>
	</table>
	<h3>Tornado (double,screw) 雙股龍捲風</h3>
	<table class="table table-striped table-hover ">
		<tr>
			<td>item</td>
			<td>needle length</td>
			<td>threads length</td>
		</tr>
		<tr>
			<td>T1 (26G)</td>
			<td>38mm</td>
			<td>50mm</td>
		</tr>
		<tr>
			<td>T2 (26G)</td>
			<td>60mm</td>
			<td>90mm</td>
		</tr>
		<tr>
			<td>T3 (26G)</td>
			<td>90mm</td>
			<td>120mm</td>
		</tr>
		<!--
		<tr>
			<td>T4 (26G)</td>
			<td>50mm</td>
			<td>70mm</td>
		</tr>
		<tr>
			<td>T5 (25G)</td>
			<td>60mm</td>
			<td>90mm</td>
		</tr>
		<tr>
			<td>T6 (25G)</td>
			<td>90mm</td>
			<td>135mm</td>
		</tr>
	-->
	</table>
	<h3>Rose (combi) 玫瑰倒勾線</h3>
	<table class="table table-striped table-hover">
		<tr>
			<td>item</td>
			<td>needle length</td>
			<td>threads length</td>
		</tr>
		<tr>
			<td>Rose1 (23G)</td>
			<td>60mm</td>
			<td>90mm</td>
		</tr>
		<tr>
			<td>Rose2 (23G)</td>
			<td>90mm</td>
			<td>120mm</td>
		</tr>
		
	</table>
	<hr />
</div>

<?php
	require '../template/tp_footer.php';
?>
<script src="<?php echo SITE_ROOT;?>js/wavelift.js"></script>
<script>
	waveliftWhichNav(4)
</script>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 水波拉提" );
		});
</script>