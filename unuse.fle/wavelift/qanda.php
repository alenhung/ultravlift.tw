<?php
	require '../include/config.php';
	//載入樣板
	//header
	require '../template/tp_site_header.php';
	//navbar
	require '../template/tp_header.php';
?>
<div class="container whiteBg">
  <ul class="breadcrumb">
    <li><a href="<?php echo SITE_ROOT; ?>">首頁</a> <span class="divider">/</span></li>
    <li><a href="<?php echo SITE_ROOT; ?>wavelift/">Reage 水波拉提術</a> <span class="divider">/</span></li>
    <li class=""> 常見問題<span class="divider">/</span></li>
    <li class="active"> 常見問題</li>
  </ul>
</div>
<div class="container whiteBg">
	<div class="row">
    <div id="contentSideMenuStyle" class="span3">
      <!--Sidebar Emnu-->
      <?php require'../template/tp_waveliftSideMenuBar.php';?>
    </div>
    <div class="span9">
      <div class="row">
        <div class="span6">
          <!--Body content-->
          <legend>常見問題</legend>
          <ul class="QandA">
            <li class="question">「Reage水波拉提術」的施行時間需要多久？？</li>
            <li class="answer">依施術部位的多少，加上前後相關處置，約一至兩個小時內即可完成。</li>
          </ul>
          <ul class="QandA">
            <li class="question">進行「Reage水波拉提術」時，會有感覺嗎？</li>
            <li class="answer">配合皮膚表面麻膏的使用，操作時幾乎不會有痛感。在特定部位或是較敏感體質，也可以配合局部麻醉施打來提高舒適感。</li>
          </ul>
          <!--
          <ul class="QandA">
            <li class="question">作完「Reage水波拉提術」後，何時可以看到效果？可以維持多久呢？</li>
            <li class="answer">在專業醫師施術後，初步效果立即可見，一至三個月後效果會更加明顯。整體效果可以持續一年多到兩年的時間。</li>
          </ul>
        -->
          <ul class="QandA">
            <li class="question">所植入的線是否會摸的出來？表情是否會不自然？</li>
            <li class="answer">所使用的線細如髮絲，幾乎摸不出來。同時利用水波狀排列拉提，不會有表情不自然的問題。</li>
          </ul>
          <ul class="QandA">  
            <li class="question">進行「Reage水波拉提術」時，可以與BOTOX、玻尿酸等其他微整形合併施行嗎？</li>
            <li class="answer">「Reage水波拉提」的效果已經相當明顯，您也可 同時合併BOTOX施打去除動態皺紋，或合併玻尿酸注射填補過度凹陷組織，不會互相干擾。</li>
          </ul>
          <ul class="QandA">  
            <li class="question">「Reage水波拉提術」，有副作用嗎？</li>
            <li class="answer">局部、輕微的瘀青是術後最常見的現象，術後可立即冰敷來降低發生率。所使用的線材6~8個月內可完全吸收，無長期滯留困擾。</li>
          </ul>
        </div>
        <div class="span3">
          <img src="<?php echo SITE_ROOT;?>img/wavelifting/qandq-right.png" alt="">
        </div>
      </div>
    	
    </div>
	</div>
</div>
	
			
<?php
	require '../template/tp_footer.php';
?>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 常見問題" );

      $('#menu5 > li').eq(0).addClass('sideMenuActive');
		});
</script>