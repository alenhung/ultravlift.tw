<?php
	require '../include/config.php';
	//載入樣板
	//header
	require '../template/tp_header.php';
	//navbar
	require '../template/tp_navbar.php';
?>
<header class="siteHeader">
	<div class="container">
		<img src="../img/site-indexlogo5.png" alt="" />
	</div>
</header>
<?php require '../template/tp_wavelift_navbar.php';?>
<div class="container contentStart">
	<table class="table table-striped table-hover">
		<tr>
			<td></td>
			<td>電波拉皮</td>
			<td>水波拉提</td>
			<td>拉皮手術</td>
		</tr>
		<tr>
			<td>效果</td>
			<td>＋</td>
			<td>＋＋</td>
			<td>＋＋＋</td>
		</tr>
		<tr>
			<td>麻醉方式</td>
			<td>靜脈麻醉</td>
			<td>表面麻膏</td>
			<td>全身麻醉</td>
		</tr>
		<tr>
			<td>恢復期</td>
			<td>中</td>
			<td>短</td>
			<td>長</td>
		</tr>
		<tr>
			<td>維持時間</td>
			<td>1~2年</td>
			<td>1~2年</td>
			<td>3~5年</td>
		</tr>
		<tr>
			<td>費用</td>
			<td>++</td>
			<td>++</td>
			<td>+++</td>
		</tr>
	</table>
	<hr />
</div>
<div class="container">
	<div class="row">
		<div class="span5">
			<img src="../img/wavelift-logo.png" alt="" />
		</div>
		<div class="span7">
			<h1>水波拉提術特色</h1>
			<ul>
				<li>專利設計：REAGE為改良彈性針頭，配合水波拉提術更具拉提效果。</li>
				<li>完全吸收：使用PDO線材，八至十二個月內安全分解，效果可持續維持兩年。</li>
				<li>簡單快速：只需塗抹皮膚表面麻膏半小時後即可進行，全臉實施時間不到一小時。</li>
				<li>效果明顯：施術後立即有效，一至三個月後效果更加顯著，膠原蛋白持續增生至線完全吸收</li>
				<li>恢復期短：術後只需冰敷，無需特別護理，不影響日常生活。</li>
				<li>量身訂做：除全面基礎拉堤外，尚可進行局部加強，效果更加顯著。</li>
				<li>合併治療：無排他性，可同時進行保妥適、玻尿酸等治療。</li>
			</ul>
		</div>
	</div>
</div>
<?php
	require '../template/tp_footer.php';
?>
<script src="<?php echo SITE_ROOT;?>js/wavelift.js"></script>
<script>
	waveliftWhichNav(5)
</script>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 水波拉提" );
		});
</script>