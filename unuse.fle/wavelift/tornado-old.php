<?php
	require '../include/config.php';
	//載入樣板
	//header
	require '../template/tp_header.php';
	//navbar
	require '../template/tp_navbar.php';
?>
<header class="siteHeader">
	<div class="container">
		<img src="../img/site-indexlogo2.png" alt="" />
	</div>
</header>

<?php require '../template/tp_wavelift_navbar.php';?>

<div class="container">
	<h4 class="grey-line3 content-top">
		<span>適用於中度老化的患者，例如法令紋、嘴角細紋、蘋果肌拉提等</span>
	</h4>
	<div class="row">
		<div class="span5">
			<img src="../img/tornado-1.png" alt="水波拉提-tornado針頭" />
		</div>
		<div class="span7 wave3">
			<h1>極致工藝 雙股纏繞高超技術</h1>
			<p>目前市場目前僅此產品為PDO雙股纏繞設計，並螺旋纏繞於針頭。入針後擁有更大面積刺激膠原蛋白增生，提高緊實除皺效果。配合水波拉提術施術針對特定部位與適應症，提供比起一般單股PDO平滑線創造數倍緊實、除皺效果。</p>
		</div>
	</div>
	<hr />
	<div class="row">
		<div class="span6">
			<div class="reage1">
				<h1>雙股螺旋龍捲風</h1>
				<p>雙股螺旋設計，目的在於提供更大體積刺激膠原蛋白增生，除了提供兩倍單股線的體積外，更纏繞螺旋於針頭上，比起線材螺旋入針後更加緊密，集中PDO刺激效果，為最強緊實、除皺的最佳利器。</p>
				
			</div>
		</div>
		<div class="span6 imageside">
				<img src="../img/tornado-2.png" alt="" />
		</div>
	</div>
	<hr />
	<div class="row">
		<div class="span6">
			<img src="../img/kfda.png" alt="" />
		</div>
		<div class="span6 wave1">
			<h1>旋風登場</h1>
			<p>韓國KFDA第四級安全認證</p>
			<p>台灣衛生署醫療器械輸入許可證</p>
			<h3>更多資訊請洽本公司各區業務經理或來電洽詢</h3>
		</div>
	</div>
	<div class="row">
		<div class="span12">
			<h3>Tornado (double,screw) 雙股龍捲風</h3>
	<table class="table table-striped table-hover ">
		<tr>
			<td>item</td>
			<td>needle length</td>
			<td>threads length</td>
		</tr>
		<tr>
			<td>T1 (26G)</td>
			<td>38mm</td>
			<td>50mm</td>
		</tr>
		<tr>
			<td>T2 (26G)</td>
			<td>60mm</td>
			<td>90mm</td>
		</tr>
		<tr>
			<td>T3 (26G)</td>
			<td>90mm</td>
			<td>120mm</td>
		</tr>
	</table>
		</div>
	</div>
		
	</div>

<?php
	require '../template/tp_footer.php';
?>
<script src="<?php echo SITE_ROOT;?>js/wavelift.js"></script>
<script>
	waveliftWhichNav(2)
</script>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 水波拉提" );
		});
</script>