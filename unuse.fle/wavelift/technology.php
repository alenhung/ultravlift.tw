<?php
	require '../include/config.php';
	//載入樣板
	//header
	require '../template/tp_site_header.php';
	//navbar
	require '../template/tp_header.php';
	//navbar
	require '../template/tp_navbar.php';
?>

	<div class="container IntroDiv whiteBg">
		<img src="../img/wavelifting/intro-wavelifting.png" alt="" />
	</div>
<?php require '../template/tp_wavelift_navbar.php';?>

<!--mono-->
<!--
<div class="container  waveIntroWord">
	<h2>完美效果來自醫師的技術與好材料的組合</h2>
	<p>特殊水波紋埋線技法與專利的彈性針頭，Reage水波拉提術利用如漣漪般的線體排列，在真皮層下形成堅固支撐，讓鬆弛的肌膚不再下垂。後續膠原蛋白的持續增生效果，可以撫平肌膚上的皺紋與毛孔。專利的彈性針頭設計，有效的降低術後瘀青與腫脹，讓施術後的恢復期縮至最短。</p>
</div>
-->
<div class="container whiteBg waveproduct">
	<div class="row">
		<div class="span6 ">
			<img src="../img/wavelifting/thread.jpg" alt="">
		</div>
		<div class="span6 teach0">
			<h2>打破一般線材拉皮只用一種細線，無法真正達到明顯拉提的效果</h2>
			<p>許多做過其他市面上線性拉提的消費者都抱怨，臉部已經埋了100~200條細線但是效果卻不明顯。原因在於這些平滑細線進入臉不時只能刺激產生膠原蛋白以及緊實肌膚的效果，但要達到真正臉部拉提的效果明顯不足，而「Reage水波拉提」的特色就是打破許多線性拉題旨用單一細線。所以針對不同老化問題，「Reage水波拉提」可以利用不同PDO線來做改善。</p>
		</div>
	</div>
	<div class="row">
		<div class="span6 teach1">
			<h2>完美效果來自醫師技術與好材料的組合</h2>
			<p>特殊水波紋埋線技法與專利的彈性針頭，Reage水波拉提術利用如漣漪般的線體排列，在真皮層下形成堅固支撐，讓鬆弛的肌膚不再下垂。後續膠原蛋白的持續增生效果，可以撫平肌膚上的皺紋與毛孔。專利的彈性針頭設計，有效的降低術後瘀青與腫脹，讓施術後的恢復期縮至最短。</p>
		</div>
		<div class="span6">
			<img src="../img/wavelifting/threeLine.png" alt="" class="">
		</div>
	</div>
</div>
<div class="container whiteBg">
	<div class="row">
		<div class="span5">
			<img src="../img/wavelifting/tech1jpg" alt="水波拉提-reage針頭" />
		</div>
		<div class="span7 teach2">
			<h2>埋線拉提原理說明</h2>
			<ul>
				<li class="orange1"><span >肌膚因膠原蛋白流失，組織支撐力下降而鬆弛。</span></li>
				<li class="orange2"><span >將PDO線依平行、多條的水波紋方式植入真皮層。</span></li>
				<li class="orange3"><span>藉異物反應，刺激自體膠原蛋白增生，增加組織支撐力，改善鬆弛。</span></li>
				<li class="orange4"><span>PDO線會被人體逐漸吸收，但增生力不會消失，持續保有拉提效果。</span></li>
			</ul>			
		</div>
	</div>
</div>
<!--產品規格-->
<div class="container whiteBg waveproduct">
	<div class="row">
		<div class="span12">
			<h2>產品技術規格表</h2>
			<h4>單股平滑線 Reage Mono Thread 規格表</h4>
			<h5>Reage</h5>
			<table class="table table-striped table-hover">
				<tr>
					<td>item</td>
					<td>Gauge</td>
					<td>USP</td>
					<td>needle length</td>
					<td>threads length</td>
				</tr>
				<tr>
					<td>R1</td>
					<td>30G</td>
					<td>7-0</td>
					<td>30mm</td>
					<td>30mm</td>
				</tr>
				<tr>
					<td>R2</td>
					<td>30G</td>
					<td>7-0</td>
					<td>40mm</td>
					<td>50mm</td>
				</tr>
				<tr>
					<td>R5</td>
					<td>29G</td>
					<td>6-0</td>
					<td>60mm</td>
					<td>90mm</td>
				</tr>
			</table>
			<hr />
			<h5>Dream UP Mono</h5>
			
			<table class="table table-striped table-hover ">
				<tr>
					<td>item</td>
					<td>Gauge</td>
					<td>USP</td>
					<td>needle length</td>
					<td>threads length</td>
				</tr>
				<tr>
					<td>D1</td>
					<td>26G</td>
					<td>5-0</td>
					<td>60mm</td>
					<td>90mm</td>
				</tr>
				<tr>
					<td>D2</td>
					<td>26G</td>
					<td>5-0</td>
					<td>90mm</td>
					<td>135mm</td>
				</tr>
				<tr>
					<td>D3</td>
					<td>27G</td>
					<td>5-0</td>
					<td>38mm</td>
					<td>50mm</td>
				</tr>
				<tr>
					<td>D4</td>
					<td>27G</td>
					<td>5-0</td>
					<td>50mm</td>
					<td>70mm</td>
				</tr>
				<tr>
					<td>D5</td>
					<td>27G</td>
					<td>5-0</td>
					<td>60mm</td>
					<td>90mm</td>
				</tr>
				<tr>
					<td>D6</td>
					<td>27G</td>
					<td>5-0</td>
					<td>90mm</td>
					<td>135mm</td>
				</tr>
				<tr>
					<td>D7</td>
					<td>29G</td>
					<td>6-0</td>
					<td>25.4mm</td>
					<td>30mm</td>
				</tr>
				<tr>
					<td>D8</td>
					<td>29G</td>
					<td>6-0</td>
					<td>38mm</td>
					<td>50mm</td>
				</tr>
				<tr>
					<td>D9</td>
					<td>29G</td>
					<td>6-0</td>
					<td>50mm</td>
					<td>70mm</td>
				</tr>
				<tr>
					<td>D10</td>
					<td>29G</td>
					<td>6-0</td>
					<td>60mm</td>
					<td>90mm</td>
				</tr>
				<tr>
					<td>D11</td>
					<td>30G</td>
					<td>7-0</td>
					<td>25mm</td>
					<td>30mm</td>
				</tr>
				<tr>
					<td>D12</td>
					<td>30G</td>
					<td>7-0</td>
					<td>38mm</td>
					<td>50mm</td>
				</tr>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="span12">
			<h4>雙股龍捲風 Reage tornado Thread 規格表</h4>
			<table class="table table-striped table-hover ">
				<tr>
					<td>item</td>
					<td>Gauge</td>
					<td>USP</td>
					<td>needle length</td>
					<td>threads length</td>
				</tr>
				<tr>
					<td>T1</td>
					<td>26G</td>
					<td>7-0 X 2</td>
					<td>38mm</td>
					<td>50mm</td>
				</tr>
				<tr>
					<td>T2</td>
					<td>26G</td>
					<td>7-0 X 2</td>
					<td>60mm</td>
					<td>90mm</td>
				</tr>
				<tr>
					<td>T3</td>
					<td>26G</td>
					<td>7-0 X 2</td>
					<td>90mm</td>
					<td>120mm</td>
				</tr>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="span12">
			<h4>玫瑰雙側倒勾 Reage Rose Thread 規格表</h4>
			<table class="table table-striped table-hover">
				<tr>
					<td>item</td>
					<td>Gauge</td>
					<td>USP</td>
					<td>needle length</td>
					<td>threads length</td>
				</tr>
				<tr>
					<td>Rose1</td>
					<td>23G</td>
					<td>3-0</td>
					<td>60mm</td>
					<td>90mm</td>
				</tr>
				<tr>
					<td>Rose2</td>
					<td>23G</td>
					<td>3-0</td>
					<td>90mm</td>
					<td>120mm</td>
				</tr>
				
			</table>
		</div>
	</div>
</div>
<?php
	require '../template/tp_footer.php';
?>
<script src="<?php echo SITE_ROOT;?>js/wavelift.js"></script>
<script>
	waveliftWhichNav(2);


</script>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 水波拉提" );
		});
</script>