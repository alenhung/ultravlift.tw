<?php
	require '../include/config.php';
	//載入樣板
	//header
	require '../template/tp_header.php';
	//navbar
	require '../template/tp_navbar.php';
?>
<header class="siteHeader">
	<div class="container">
		<img src="../img/site-indexlogo4.png" alt="" />
	</div>
</header>

<?php require '../template/tp_wavelift_navbar.php';?>

<div class="container">
	<h4 class="grey-line3 content-top">
		<span>適用於輕度老化的患者，主要透過膠原蛋白增生來緊實肌膚，讓肌膚看起來彈性飽滿</span>
	</h4>
	<div class="row">
		<div class="span5">
			<img src="../img/reage1.png" alt="水波拉提-reage針頭" />
		</div>
		<div class="span7 wave3">
			<h1>獨具特色 業界同規格中最細針頭</h1>
			<p>目前市場同規格中唯有REAGE為30G針頭，極細的針頭利於施術的醫師入針更順利，並且將可減少施術過程中所造成的針孔傷口與瘀青產生。配合水波拉提術施術所造成的針孔傷口將於半小時左右即可消失。</p>
		</div>
	</div>
	<hr />
	<div class="row">
		<div class="span6">
			<div class="reage1">
				<h1>極俱彈性與針頭特殊設計</h1>
				<p>傳統針頭的直線切割方式，雖便利入針順利但也容易刺破微血管造成出血。REAGE特殊導角切割與極俱彈性的設計，於施術過程中將有效避開血管，降低術後瘀清機率。因此成為水波拉提術不可或缺的利器。</p>
				
			</div>
		</div>
		<div class="span6 imageside">
				<img src="../img/reage3.png" alt="水波拉提-reage針頭特殊設計" />
		</div>
	</div>
	<hr />
	<div class="row">
		<div class="span6">
			<img src="../img/kfda.png" alt="" />
		</div>
		<div class="span6 wave1">
			<h1>安全與認證</h1>
			<p>韓國KFDA第四級安全認證。</p>
			<p>Patent No. 0473108 Approved Item No. 11-172</p>
			<p>台灣衛生署醫療器械輸入許可證</p>
			<p>衛署醫器輸壹字號第012523號</p>
		</div>
	</div>
	<hr />
	<div class="row">
		<div class="span12">
			<h1>Reage系列</h1>
	<table class="table table-striped table-hover">
		<tr>
			<td>item</td>
			<td>neddle length</td>
			<td>threads length</td>
		</tr>
		<tr>
			<td>R1 (30G)</td>
			<td>30mm</td>
			<td>30mm</td>
		</tr>
		<tr>
			<td>R2 (30G)</td>
			<td>40mm</td>
			<td>50mm</td>
		</tr>
		<tr>
			<td>R5 (29G)</td>
			<td>60mm</td>
			<td>90mm</td>
		</tr>
	</table>
	<hr />
			<h1>Dream UP系列</h1>
	<h3>Mono 平滑線</h3>
	<table class="table table-striped table-hover ">
		<tr>
			<td>item</td>
			<td>Gauge</td>
			<td>USP</td>
			<td>needle length</td>
			<td>threads length</td>
		</tr>
		<tr>
			<td>D1</td>
			<td>26G</td>
			<td>5-0</td>
			<td>60mm</td>
			<td>90mm</td>
		</tr>
		<tr>
			<td>D2</td>
			<td>26G</td>
			<td>5-0</td>
			<td>90mm</td>
			<td>135mm</td>
		</tr>
		<tr>
			<td>D3</td>
			<td>27G</td>
			<td>5-0</td>
			<td>38mm</td>
			<td>50mm</td>
		</tr>
		<tr>
			<td>D4</td>
			<td>27G</td>
			<td>5-0</td>
			<td>50mm</td>
			<td>70mm</td>
		</tr>
		<tr>
			<td>D5</td>
			<td>27G</td>
			<td>5-0</td>
			<td>60mm</td>
			<td>90mm</td>
		</tr>
		<tr>
			<td>D6</td>
			<td>27G</td>
			<td>5-0</td>
			<td>90mm</td>
			<td>135mm</td>
		</tr>
		<tr>
			<td>D7</td>
			<td>29G</td>
			<td>6-0</td>
			<td>25.4mm</td>
			<td>30mm</td>
		</tr>
		<tr>
			<td>D8</td>
			<td>29G</td>
			<td>6-0</td>
			<td>38mm</td>
			<td>50mm</td>
		</tr>
		<tr>
			<td>D9</td>
			<td>29G</td>
			<td>6-0</td>
			<td>50mm</td>
			<td>70mm</td>
		</tr>
		<tr>
			<td>D10</td>
			<td>29G</td>
			<td>6-0</td>
			<td>60mm</td>
			<td>90mm</td>
		</tr>
		<tr>
			<td>D11</td>
			<td>30G</td>
			<td>7-0</td>
			<td>25mm</td>
			<td>30mm</td>
		</tr>
		<tr>
			<td>D12</td>
			<td>30G</td>
			<td>7-0</td>
			<td>38mm</td>
			<td>50mm</td>
		</tr>
	</table>
	
		</div>
	</div>
		
	</div>

<?php
	require '../template/tp_footer.php';
?>
<script src="<?php echo SITE_ROOT;?>js/wavelift.js"></script>
<script>
	waveliftWhichNav(1)
</script>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 水波拉提" );
		});
</script>