<?php
	require '../include/config.php';
	//載入樣板
	//header
	require '../template/tp_site_header.php';
	//navbar
	require '../template/tp_header.php';
	//navbar
	//require '../template/tp_navbar.php';
?>
<!--
	<div class="container IntroDiv whiteBg">
		<img src="../img/wavelifting/intro-wavelifting.png" alt="" />
	</div>
-->
<div class="container whiteBg">
	<?php //require '../template/tp_breadcrumb.php'; ?>
  <ul class="breadcrumb">
    <li><a href="<?php echo SITE_ROOT; ?>">首頁</a> <span class="divider">/</span></li>
    <li><a href="<?php echo SITE_ROOT; ?>wavelift/">Reage 水波拉提術</a> <span class="divider">/</span></li>
    <li class=""> Reage 線材規格<span class="divider">/</span></li>
    <li class="active"> 倒勾線 Rose thread</li>
  </ul>
</div>
<div class="container whiteBg">
  <div class="row">
    <div id="contentSideMenuStyle" class="span3">
      <!--Sidebar Emnu-->
      <?php require'../template/tp_waveliftSideMenuBar.php';?>
    </div>
    <div class="span9">
      <!--Body content-->
      <legend>倒勾線 Rose thread</legend>
      <div class="herointo">
        <p>適合局部加強用，提供最強拉提力。適用於重度老化患者。</p>
      </div>
      <div class="row">
      	<div class="span9">
      		<img src="../img/rose-1.png" alt="水波拉提-reage針頭" />

          <img src="../img/rose-2.png" alt="水波拉提-reage針頭" />
      	</div>
      </div>
      <!---->
			<div class="row">
				<div class="span6">
          <div id="reage2">  
            <h3>倒勾線的祕密</h3>
            <p>
              市面上大部分的倒勾線，只有外側線體才有倒勾設計，針內看不到的線體仍為一般的單股平滑線。<br/>
              Reage的倒勾Rose線，不管針內外線體都擁有倒勾設計，提供雙側、加倍的物理拉力。
            </p>
          </div>
        </div>
				<div class="span3"><img src="../img/rose-icon1.png" alt=""></div>
			</div>
      <div class="row">
        <div class="span9">
          <h3>Rose thread 規格表</h3>
          <table class="table table-striped table-hover">
            <tr>
              <td>item NO.</td>
              <td>Gauge</td>
              <td>USP</td>
              <td>Needle Length</td>
              <td>Thread Length</td>
            </tr>
            <tr>
              <td>Rose1</td>
              <td>23G</td>
              <td>4-0</td>
              <td>60mm</td>
              <td>90mm</td>
            </tr>
            <tr>
              <td>Rose2</td>
              <td>23G</td>
              <td>4-0</td>
              <td>80mm</td>
              <td>120mm</td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
	require '../template/tp_footer.php';
?>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 倒勾線 Rose thread" );
      $('#menu2 > li').eq(2).addClass('sideMenuActive');
		});
</script>