<?php
	require '../include/config.php';
	//載入樣板
	//header
	require '../template/tp_site_header.php';
	//navbar
	require '../template/tp_header.php';
	//navbar
	//require '../template/tp_navbar.php';
?>
<!--
	<div class="container IntroDiv whiteBg">
		<img src="../img/wavelifting/intro-wavelifting.png" alt="" />
	</div>
-->
<div class="container whiteBg">
	<?php //require '../template/tp_breadcrumb.php'; ?>
  <ul class="breadcrumb">
    <li><a href="<?php echo SITE_ROOT;?>">首頁</a> <span class="divider">/</span></li>
    <li><a href="<?php echo SITE_ROOT; ?>wavelift/">Reage 水波拉提術</a> <span class="divider">/</span></li>
    <li class=""> Reage 水波拉提指南<span class="divider">/</span></li>
    <li class="active"> 術後問題與護理</li>
  </ul>
</div>
<div class="container whiteBg">
  <div class="row">
    <div id="contentSideMenuStyle" class="span3">
      <!--Sidebar Emnu-->
      <?php require'../template/tp_waveliftSideMenuBar.php';?>
    </div>
    <div class="span9">
      <div class="row">
        <div class="span6">
          <legend>
            術後問題與護理
          </legend>
          <!--Body content-->
          
          
          <h4>針頭與PDO線是Reage水波拉提術的主材料，術後最常見的問題與護理重點與一般注射項目類似。</h4>
          <!---->
          <div id="care">
            <p class="headerList">針孔痕跡</p>
            <p class="careContent">大部分的針孔痕跡，一般半小時內即會消失，不需擔心疤痕問題。</p>
            <p class="headerList">瘀青與腫脹</p>
            <p class="careContent">瘀青是最常見的術後問題，術後立即冰敷與良好的醫師操作手法可以有效地降低術後瘀青與腫脹之發生率與程度，至少持續三天是需要的。一般亦會伴隨程度輕微的腫脹。但均會隨著時間而緩解。</p>
            <p class="headerList">局部痛感</p>
            <p class="careContent">剛接受完治療後數日內，當重壓治療區時，輕微的痛感是正常的，會隨時間而緩解。</p>
            <p class="headerList">線頭突出</p>
            <p class="careContent">有極少數的治療者接受治療後會有線頭移位或突出的現象，良好的醫師操作手法是關鍵。建議術後數日內儘可能減少臉部大動作的表情，以減少發生率。</p>
          </div>
        </div>
        <div class="span3">
          <img src="<?php echo SITE_ROOT;?>img/wavelifting/care-right.png" alt="">
        </div>
      </div>
      
      
        
      </div>
      

        
      <!---->
      
    </div>
  </div>
</div>
<?php
	require '../template/tp_footer.php';
?>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 術後問題與護理" );
      $('#menu1 > li').eq(2).addClass('sideMenuActive');
		});
</script>