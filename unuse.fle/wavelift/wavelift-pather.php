<?php
	require '../include/config.php';
	//載入樣板
	//header
	require '../template/tp_header.php';
	//navbar
	require '../template/tp_navbar.php';
?>
<header class="siteHeader">
	<div class="container">
		<img src="../img/site-indexlogo5.png" alt="" />
	</div>
</header>
<?php require '../template/tp_wavelift_navbar.php';?>
<div class="container contentStart">
	<div class="row">
		<div class="span12">
			<h3>治療範圍：V臉拉提。</h3>
			<img src="../img/case3.png" alt="水波拉提治療範圍：V臉拉提">
			<h3>治療範圍：法令紋、嘟嘟嘴、木偶紋</h3>
			<img src="../img/case1.png" alt="水波拉提治療範圍：法令紋、嘟嘟嘴、木偶紋">
			<h3>治療範圍：額頭靜態紋路、抬頭紋、眉間紋路、眼睛下垂、眼下細紋、魚尾紋。</h3>
			<img src="../img/case2.png" alt="水波拉提治療範圍圍：額頭靜態紋路、抬頭紋、眉間紋路、眼睛下垂、眼下細紋、魚尾紋">
			<h3>治療範圍：眉間紋路。</h3>
			<img src="../img/case5.png" alt="水波拉提治療範圍：眉間紋路">
			<h3>治療範圍：下顎線、雙下巴。</h3>
			<img src="../img/case4.png" alt="水波拉提治療範圍：下顎線、雙下巴">
			<h3>治療範圍：雙下巴。</h3>
			<img src="../img/case7.png" alt="水波拉提治療範圍：雙下巴">
			<h3>治療範圍：頸紋。</h3>
			<img src="../img/case6.png" alt="水波拉提治療範圍：頸紋">
		</div>
	</div>
</div>
<?php
	require '../template/tp_footer.php';
?>
<script src="<?php echo SITE_ROOT;?>js/wavelift.js"></script>
<script>
	waveliftWhichNav(7)
</script>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 水波拉提" );
		});
</script>