<?php
require 'include/config.php';
//載入樣板
//header
require 'template/tp_header.php';
//navbar
require 'template/tp_navbar.php';
?>
<!--CONTENT START-->
<header class="siteHeader">
	<div id="container contentStart">

	</div>
</header>
<div class="container">
	<div class="row">
		<div class="span6">
			<form action="action/modify.php?type=contact" method="post" accept-charset="utf-8" class="form-horizontal">
				<fieldset>
					<legend>
						聯絡我們
					</legend>
					<div class="control-group require">
						<label class="control-label">院所名稱：</label>
						<div class="controls ">
							<input type="text" placeholder="請輸入院所名稱" class="span4" name="companyName" id="companyName">
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">聯絡人：</label>
						<div class="controls">
							<input type="text" placeholder="請輸入聯絡人名稱" class="span4" name="contactName" id="contactName">
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">聯絡電話：</label>
						<div class="controls">
							<input type="tel" placeholder="請輸入聯絡電話" class="span4" name="contactTel" id="tel">
							<span class="help-inline"></span>
						</div>
					</div>
					<div class="control-group require">
						<label class="control-label">聯絡信箱：</label>
						<div class="controls">
							<input type="email" placeholder="請輸入聯絡信箱" class="span4" name="contactEmail" id="email">
							<span class="help-inline" id="wrongEmail"></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">諮詢內容：</label>
						<div class="controls">
							<textarea rows="3" class="span4" name="comment" id="comment"></textarea>
						</div>
					</div>
					<div class="control-group">
    					<div class="controls">
    						<hr />
    						<button type="submit" class="btn pull-right">送出</button>
    						<span class="help-inline"></span>
    					</div>
    				</div>	
				</fieldset>
			</form>
		</div>
		<div class="span6">
			
			<div id="contactDIV">
				<h3>感謝您的聯繫！</h3>
				<p>我們將儘速處理與回覆您的問題！</p>
			</div>
			<hr />
			<div id="contactINFO" >
				<!-- <img src="img/logo_haoshan.png" alt="" class="pull-right"/> -->
				<h4>浩善生物科技股份有限公司 </h4>
				<h5>Hao Shan Biotechnology Co.,Ltd</h5>
				<div id="contactAddress">
					<p>聯絡電話：04-2326-8822</p>
					<p>聯絡信箱：<a href="mailto:service@haoshan.com.tw">service@haoshan.com.tw</a></p>
					<p>通訊地址：台中市台灣大道二段715號4樓之2</p>
				</div>
			</div>
		</div>
	</div>
</div>
<!--CONTENT END-->
<?php
require 'template/tp_footer.php';
?>
<script src="js/jquery.numeric.js"></script>
<script>
$(document).ready(function(){
	$( 'title' ).html ( "<?php echo SITE_NAME;?> - 聯絡我們" );
	$('#tel').numeric();
	$('#email').change(function(){
		if(validateEmail($(this).val())){
			$('#email').parent().parent().removeClass('error');
			$('#wrongEmail').removeClass('inputWrong');
			$('#wrongEmail').text('');
		}else{
			
		}	
	});
	$('form').submit(function(){
		if(inputEmpty() >= 1){
			return false;
		}
		if(validateEmail($('#email').val())){
			return true;
			}else{
				$('#email').parent().parent().addClass('error');
				$('#wrongEmail').text('E-mail 格式錯誤');
			return false;
		};
	});
});
</script>