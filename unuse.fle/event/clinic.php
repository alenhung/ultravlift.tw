<?php
  require '../include/config.php';
  //載入樣板
  //header
  require '../template/tp_site_header.php';
  //navbar
  require '../template/tp_header.php';
?>
<link type="text/css" rel="stylesheet" href="../css/bootstrap-lightbox.min.css"></link>
<div class="container whiteBg">
  <ul class="breadcrumb">
    <li><a href="<?php echo SITE_ROOT;?>">首頁</a> <span class="divider">/</span></li>
    <li><a href="<?php echo SITE_ROOT;?>seminar/">研習會活動</a> <span class="divider">/</span></li>
    <li class="active">活動花絮</li>
  </ul>
</div>
<div class="container whiteBg">
	<div class="row">
		<div class="span3">
      <!--Sidebar Emnu-->
      <?php require'../template/tp_seminarSideMenuBar.php';?>
    </div>
		<div class="span9">
        <legend>
        診所一對一教學<span class="seminarDate"></span>
      </legend>
				<div class="heroIntro2">
          <p>
            內容整理中，敬請期待。
          </p>
      </div>
		</div>
	</div>
	
	
</div>
<?php
	require '../template/tp_footer.php';
?>

<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 診所一對一教學記錄" );

      $('.sideSubMenu').find('li').eq(3).addClass('sideMenuActive');
		});
</script>