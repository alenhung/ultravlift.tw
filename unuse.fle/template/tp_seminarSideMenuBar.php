<div id="contentSideMenuStyle">
  <ul id="sideMenu">
    
    <ul class="sideSubMenu">
      <li class="">
        <a href="<?php echo SITE_ROOT; ?>seminar/">課程安排</a>
      </li>
      <li>
        <a href="<?php echo SITE_ROOT; ?>seminar/calendar.php">研習會時間表</a>
      </li>
      <li class="">
        <a href="<?php echo SITE_ROOT; ?>seminar/singUp.php">報名方式</a>
      </li>
      <li class="">
        <a href="<?php echo SITE_ROOT; ?>event/">活動花絮</a>
        
      </li>
      <ul id="eventSubMenu">
          <li><a href="<?php echo SITE_ROOT; ?>event/clinic.php">診所一對一教學</a></li>
          <li><a href="<?php echo SITE_ROOT; ?>event/">中區研習會2013/04/21</a></li>
          <li><a href="<?php echo SITE_ROOT; ?>event/seminar-2013-03-24.php">北區研習會2013/03/24</a></li>
          <li><a href="<?php echo SITE_ROOT; ?>event/seminar-2013-02-24.php">中區研習會2013/02/24</a></li>
      </ul>
      
    </ul>
    
  </ul>
</div>