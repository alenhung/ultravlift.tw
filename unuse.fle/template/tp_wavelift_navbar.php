<div class="container whiteBg waveLiftNav">	
	<div class="navbar">
		<div class="navbar-inner pull-right" id="waveliftNAV">
		    <ul class="nav">
				<li class=""><a href="<?php echo SITE_ROOT?>wavelift/">簡介</a></li>
				<li class=" "><a href="<?php echo SITE_ROOT?>wavelift/product.php">材料說明</a></li>
				<li class=" "><a href="<?php echo SITE_ROOT?>wavelift/technology.php">技術說明</a></li>
				<li class=" "><a href="<?php echo SITE_ROOT?>wavelift/case.php">效果說明</a></li>
				<li class=" "><a href="<?php echo SITE_ROOT?>wavelift/different.php">與其他拉提術比較</a></li>
				<!-- <li class=" "><a href="<?php echo SITE_ROOT?>wavelift/waveqanda.php">Q & A</a></li> -->
				<!-- <li class="divider-vertical "><a href="<?php echo SITE_ROOT?>wavelift/wavelift-dm.php">線上DM</a></li> -->
		    </ul>
		</div>
	</div>
</div>