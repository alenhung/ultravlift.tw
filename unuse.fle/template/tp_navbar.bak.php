
<!-- <div class="container whiteBg"> -->

	<div class="navbar navbar-haoshan">
		<div class="navbar-inner">
			<div class="container">
				<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div class="nav-collapse">
				<ul class="nav ">
					
					
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#" data-delay="1000" data-close-others="true">Reage 水波拉提術<b class="caret"></b></a>

						<ul class="dropdown-menu">
							<li>
								<a href="<?php echo SITE_ROOT; ?>wavelift/">Reage 水波拉提術簡介</a>
							</li>
							<li>
								<a href="<?php echo SITE_ROOT; ?>wavelift/product.php">Reage 水波拉提-材料說明</a>
							</li>
							<li>
								<a href="<?php echo SITE_ROOT; ?>wavelift/technology.php">Reage 水波拉提-技術說明</a>
							</li>
							<li>
								<a href="<?php echo SITE_ROOT; ?>wavelift/case.php">Reage 水波拉提-效果說明</a>
							</li>
							
						</ul>
					</li>
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#" data-delay="1000" data-close-others="true">技術團隊<b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li>
								<a href="<?php echo SITE_ROOT; ?>wavelift/">顧問醫師群</a>
							</li>
							<li>
								<a href="<?php echo SITE_ROOT; ?>wavelift/reage.php">認證醫師群</a>
							</li>
						</ul>
					</li>
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#" data-delay="1000" data-close-others="true">教學研習會<b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li>
								<a href="<?php echo SITE_ROOT; ?>wavelift/">線上報名表</a>
							</li>
							<li>
								<a href="<?php echo SITE_ROOT; ?>wavelift/reage.php">教學研習內容</a>
							</li>
							<li>
								<a href="<?php echo SITE_ROOT; ?>wavelift/reage.php">教學研習會時間表</a>
							</li>
							<li>
								<a href="<?php echo SITE_ROOT; ?>wavelift/reage.php">教學研習會花絮記錄</a>
							</li>
						</ul>
					</li>
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#" data-delay="1000" data-close-others="true">案例介紹<b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li>
								<a href="<?php echo SITE_ROOT; ?>wavelift/">小V臉拉提</a>
							</li>
							<li>
								<a href="<?php echo SITE_ROOT; ?>wavelift/reage.php">蘋果肌拉提</a>
							</li>
							<li>
								<a href="<?php echo SITE_ROOT; ?>wavelift/reage.php">眉間皺紋</a>
							</li>
							<li>
								<a href="<?php echo SITE_ROOT; ?>wavelift/reage.php">雙下巴</a>
							</li>
							<li>
								<a href="<?php echo SITE_ROOT; ?>wavelift/reage.php">鼻部塑型</a>
							</li>
							<li>
								<a href="<?php echo SITE_ROOT; ?>wavelift/reage.php">法令紋</a>
							</li>
							<li>
								<a href="<?php echo SITE_ROOT; ?>wavelift/reage.php">頸紋</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="<?php echo SITE_ROOT;?>wavelift/wavelift-pather.php">常見問題</a>
					</li>	
				<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#" data-delay="1000" data-close-others="true">聯絡我們<b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li>
								<a href="<?php echo SITE_ROOT; ?>wavelift/">醫療人員諮詢專頁</a>
							</li>
							<li>
								<a href="<?php echo SITE_ROOT; ?>wavelift/reage.php">一般民眾諮詢專頁</a>
							</li>
						</ul>
					</li>
					<!--
					<li>
						<a href="http://www.facebook.com/pages/%E6%B0%B4%E6%B3%A2%E6%8B%89%E6%8F%90/304628252973121" target="_blank" class="nav-image"><img src="<?php echo SITE_ROOT;?>img/facebook-logo2.png" alt="水波拉提官方粉絲團"></a>
					</li>
					-->
					
				</ul>
			</div>
		</div>
	</div>

	</div>
<!-- </div> -->