<?php
	require '../include/config.php';
	//載入樣板
	//header
	require '../template/tp_site_header.php';
	//navbar
	require '../template/tp_header.php';
?>

<div class="container whiteBg">
	<div class="row">
		<div class="span3">
			<!--Sidebar Emnu-->
     
		</div>
		<div class="span9">
				<!---->
				<legend>
					課程安排
				</legend>
				<!---->
				<div class="row">
					<div class="span7">
						<div class="heroIntro2">
							<p>
								Reage 水波拉提術目前研習內容分成「臉部初階研習會」與「臉部進階研習會」，其內容如下：
							</p>
						</div>
						<div class="seminarDocument">
							<h4>臉部初階研習會課程內容－採公開報名。</h4>
							<ul>
								<li>Reage水波拉提術的原理介紹。</li>
								<li>Reage水波拉提術的材料介紹與操作技巧。</li>
								<li>中1/3臉部與下1/3臉部基礎拉提。</li>
									<ol>
										<li>瘦臉與小V臉拉提的作法。</li>
										<li>蘋果肌與臉頰下垂的拉提。</li>
										<li>法令紋與嘴角紋的加強拉提。</li>
									</ol>
							</ul>
							<hr>
						</div>
						<div class="seminarDocument">

							<h4>臉部進階研習會課程內容--採邀請制，暫不接受公開報名。</h4>
							<ul>
								<li>內容：</li>
								<ol>
									<li>前額靜態紋填補與前額拉提。</li>
									<li>眉間靜態紋填補與皺眉紋的處理。</li>
									<li>鼻部橫紋的提補與埋線隆鼻。</li>
									<li>眼尾下垂與眉毛下垂的拉提。</li>
									<li>雙下巴與頸部拉提。</li>
									<li>雙股線與倒勾線的介紹。</li>
									<li>單股線、雙股線、倒勾線混合使用的中、下1/3臉部加強拉提作法。</li>
								</ol>
							</ul>
						</div>
					</div>
					<div class="span2">
						<img src="../img/seminar/left-panel.png">
					</div>
				</div>
			<!---->
		</div>
	</div>
</div>
<?php
require '../template/tp_footer.php';
?>
<script>
		$(document).ready(function(){
			$( 'title' ).html ( "<?php echo SITE_NAME;?> - 課程安排" );
			$('.sideSubMenu').find('li').eq(0).addClass('sideMenuActive');
		});
</script>